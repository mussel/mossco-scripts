function [atmesh] = read_atmesh()

    basename = 'wind_atm_fin_ch_time_vec';
    if exist([basename '.mat']) 
        load([basename '.mat'])
        return
    end

    ncid = netcdf.open([basename '.nc'],'NC_NOWRITE');
    varid = netcdf.inqVarID(ncid,'time');
    timeunit=split(netcdf.getAtt(ncid,varid,'units'),' ');
    
    atmesh.time = double(netcdf.getVar(ncid, varid)) ...
         + datenum(timeunit(3));  
    
    atmesh.u = double(netcdf.getVar(ncid, netcdf.inqVarID(ncid,'uwnd')));
    atmesh.v = double(netcdf.getVar(ncid, netcdf.inqVarID(ncid,'vwnd')));
    atmesh.pressure = double(netcdf.getVar(ncid, netcdf.inqVarID(ncid,'P')));
    atmesh.lon = double(netcdf.getVar(ncid, netcdf.inqVarID(ncid,'longitude')));
    atmesh.lat = double(netcdf.getVar(ncid, netcdf.inqVarID(ncid,'latitude')));
    atmesh.connectivity = netcdf.getVar(ncid, netcdf.inqVarID(ncid,'tri'));
   
    netcdf.close(ncid);
    save([basename '.mat'], 'atmesh')
end 

