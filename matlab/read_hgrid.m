function [lon,lat,depth,connectivity] = read_hgrid(varargin)

if nargin < 1
  filename = 'hgrid.ll';
else
  filename = varargin{1};
end

[filepath, basename, ~] = fileparts(filename);
matname = fullfile(filepath, [basename '.mat']);

if exist(matname)
  load(matname);
  return
end

fid=fopen(filename,'r');
char=fgetl(fid); %ignore
    char = split(fgetl(fid));     
    ne=str2num(char{1});
    np=str2num(char{2});
    hgrid(1:np,1:4)=nan;
    connectivity(1:ne,1:5)=nan;
    for i=1:np
        tmp=str2num(fgetl(fid));
        lon(i)=tmp(2);
        lat(i)=tmp(3);
        depth(i)=tmp(4);
    end 
    for i=1:ne
        string = fgetl(fid);
        tmp=str2num(string);
        if length(tmp) > 5 
             connectivity(i,1:5)=tmp(2:6);
        elseif length(tmp) > 4 
            connectivity(i,1:4)=tmp(2:5);
        else
            printf('%d %s',i,string);
            
        end
    end %for i
    fclose(fid);
    lon=lon';
    lat=lat';
    depth=depth';
    save(matname,'lon','lat','depth','connectivity')
