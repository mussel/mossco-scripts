function [hgrid]=cl_search_hgrid(filename, varargin)
% 
cl_register_function();

arguments = {...
  {'path','..'},...
};

[args,]=clp_arguments(varargin,arguments);
for i=1:args.length   
  eval([ args.name{i} ' = ' clp_valuestring(args.value{i}) ';']); 
end

%if ~exist(filename, 'var')
    filename = '/Users/Lemmen/devel/schism/schism_verification_tests/Test_CORIE/outputs/schout_000000_5.nc';
%end

if ~exist(filename, 'file'); return; end


[filepath,basename,~] = fileparts(filename);

hgrid = fullfile(filepath, path, 'hgrid.ll');
return
end

  
