clear all; close all;

variables = struct(...
    'stress', struct('max',7,'min', -7, 'unit', 'kg s^3'));

df = read_ww3data();
df.stress = log10(squeeze(sqrt(df.syy.^2 +  df.sxx.^2 + df.sxy.^2)));

scale=1e-6; %scale to fit

figure(1);
varlist = {'stress'};

for iv = 1:length(varlist)
    
    varname = varlist{iv};
    v = VideoWriter(['ww3data_' varname '.avi']);
    open(v);    
    data = getfield(df, varname);
    
    for it=1:numel(df.time)
      
        patch('Faces',df.connectivity','Vertices',[df.lon df.lat],'FaceVertexCData',data(:,it),'FaceColor','interp','EdgeColor','none');
        set(gca,'nextplot','add'); 
        quiver(df.lon,df.lat,df.sxx(:,it)*scale,df.syy(:,it)*scale,0,'k');
        title([varname ' ' datestr(df.time(it))]);
        set(gca,'nextplot','replacechildren'); 
        
        if it == 1
            axis tight manual             
            caxis([getfield(variables,varname).min, getfield(variables,varname).max]);
            colorbar;
        end
        
        writeVideo(v,getframe(gcf));
    end
    close(v);
end
