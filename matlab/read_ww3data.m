function [ww3] = read_ww3()

    basename = 'ww3.Constant.20151214_sxy_ike_date';
    if exist([basename '.mat']) 
        load([basename '.mat'])
        return
    end

    ncid = netcdf.open([basename '.nc'],'NC_NOWRITE');
    varid = netcdf.inqVarID(ncid,'time');
    timeunit=split(netcdf.getAtt(ncid,varid,'units'),' ');
    
    ww3.time = double(netcdf.getVar(ncid, varid)) ...
         + datenum(timeunit(3));  
    
    ww3.lon = double(netcdf.getVar(ncid, netcdf.inqVarID(ncid,'longitude')));
    ww3.lat = double(netcdf.getVar(ncid, netcdf.inqVarID(ncid,'latitude')));
    ww3.sxx = double(netcdf.getVar(ncid, netcdf.inqVarID(ncid,'sxx')));
    ww3.syy = double(netcdf.getVar(ncid, netcdf.inqVarID(ncid,'syy')));
    ww3.sxy = double(netcdf.getVar(ncid, netcdf.inqVarID(ncid,'sxy')));
    ww3.connectivity = netcdf.getVar(ncid, netcdf.inqVarID(ncid,'tri'));
   
    netcdf.close(ncid);
    save([basename '.mat'], 'ww3')
end 

