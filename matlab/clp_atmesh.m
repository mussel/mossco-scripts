clear all; close all;

variables = struct('v', struct('max',75,'min', 0, 'unit', 'm s-1'), ...
    'u', struct('max',75,'min', 0, 'unit', 'm s-1'), ...
    'speed', struct('max',100,'min', 0, 'unit', 'm s-1'), ...
    'pressure', struct('max',100150,'min', 100000, 'unit', 'Pa'));

df = read_atmesh();
df.speed = squeeze(sqrt(df.u.^2 +  df.v.^2));

scale=0.0008; %scale to fit

figure(1);
varlist = {'speed'};

for iv = 1:length(varlist)
    
    varname = varlist{iv};
    v = VideoWriter(['atmesh_' varname '.avi']);
    open(v);    
    data = getfield(df, varname);
    
    for it=1:numel(df.time)
      
        patch('Faces',df.connectivity','Vertices',[df.lon df.lat],'FaceVertexCData',data(:,it),'FaceColor','interp','EdgeColor','none');
        hold on;
        quiver(df.lon,df.lat,df.u(:,it)*scale,df.v(:,it)*scale,0,'k');
        title([varname ' ' datestr(df.time(it))]);
        hold off;
        
        if it == 1
            axis tight manual 
            set(gca,'nextplot','replacechildren'); 
            caxis([getfield(variables,varname).min, getfield(variables,varname).max]);
            colorbar;
        end
        
        writeVideo(v,getframe(gcf));
    end
    close(v);
end


figure(2);
varlist = {'u','v','pressure'};

for iv = 1:length(varlist)
    
    varname = varlist{iv};
    v = VideoWriter(['atmesh_' varname '.avi']);
    open(v);    
    data = getfield(df, varname);
    
    for it=1:numel(df.time)
  
        patch('Faces',df.connectivity','Vertices',[df.lon df.lat],'FaceVertexCData',data(:,it),'FaceColor','interp','EdgeColor','none');
        title([varname ' ' datestr(df.time(it))]);
        
        if it == 1
            axis tight manual 
            set(gca,'nextplot','replacechildren'); 
            caxis([getfield(variables,varname).min, getfield(variables,varname).max]);
            colorbar;
        end
        
        writeVideo(v,getframe(gcf));
    end
    close(v);
end
