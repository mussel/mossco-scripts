function [schout] = read_schout(varargin)

    if nargin < 1
       basename = 'outputs/schout_000000_1';
    else 
       basename = varargin{1};
    end
    
    if exist([basename '.mat']) 
        load([basename '.mat'])
        return
    end

    ncid = netcdf.open([basename '.nc'],'NC_NOWRITE');
    varid = netcdf.inqVarID(ncid,'time');
    schout.time = double(netcdf.getVar(ncid, varid));  
    %uv = double(netcdf.getVar(ncid, netcdf.inqVarID(ncid,'wind_speed')));
   
    %schout.u = squeeze(uv(1,:,:));
    %schout.v = squeeze(uv(2,:,:));
    %schout.pressure = double(netcdf.getVar(ncid, netcdf.inqVarID(ncid,'air_pressure')));
    schout.temperature = double(netcdf.getVar(ncid, netcdf.inqVarID(ncid,'temp')));
   
    netcdf.close(ncid);
    save([basename '.mat'], 'schout')
end 

