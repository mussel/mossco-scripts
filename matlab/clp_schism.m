clear all; close all;

schout_file = '/Users/Lemmen/setups/spiekeroog-transect-schism-setup/outputs/schout_000000_9';
hgrid_file = '/Users/Lemmen/setups/spiekeroog-transect-schism-setup/hgrid.ll';

cl_register_function();

variables = struct('v', struct('max',75,'min', 0, 'unit', 'm s-1'), ...
    'u', struct('max',75,'min', 0, 'unit', 'm s-1'), ...
    'speed', struct('max',100,'min', 0, 'unit', 'm s-1'), ...
    'pressure', struct('max',100150,'min', 100000, 'unit', 'Pa'), ...
    'temperature', struct('max',15,'min', 5, 'unit', '°C'), ...
    'depth', struct('max', 200, 'min', 0, 'unit', 'm') ...
    );

%df = read_schout('/Users/Lemmen/Downloads/schism1/schout_000000_1');
%df = read_schout('/Users/Lemmen/Downloads/atmesh_schism/schout_000000_1');
df = read_schout(schout_file);

[df.lon,df.lat,df.depth,df.connectivity] = read_hgrid(hgrid_file);

%df.speed = squeeze(sqrt(df.u.^2 +  df.v.^2));
df.connectivity=df.connectivity(:,2:4);
df.time = df.time/86400.0 + datenum('2008-08-24');


[lonlimit,latlimit]=clp_basemap('lonlim',cl_minmax(df.lon), ...
                'latlim',cl_minmax(df.lat));

[df.x,df.y]=m_ll2xy(df.lon,df.lat,'clip','off');

varlist= {'depth'};



for iv = 1:length(varlist)

    varname = varlist{iv};
    v = VideoWriter(['schout_' varname '.avi']);
    open(v);
    data = getfield(df, varname);

    patch('Faces',df.connectivity,'Vertices',[df.x df.y],'FaceVertexCData',data,'FaceColor','interp','EdgeColor','none');
    title([varname]);

    axis tight manual
    set(gca,'nextplot','replacechildren');
    caxis([getfield(variables,varname).min, getfield(variables,varname).max]);
    colorbar;
    
    writeVideo(v,getframe(gcf));
    close(v);
end

scale=0.0008; %scale to fit

[lonlimit,latlimit]=clp_basemap('lonlim',cl_minmax(df.lon), ...
                'latlim',cl_minmax(df.lat));

varlist = {} %'speed'};

for iv = 1:length(varlist)

    varname = varlist{iv};
    v = VideoWriter(['schout_' varname '.avi']);
    open(v);
    data = getfield(df, varname);

    for it=1:100:numel(df.time)

        patch('Faces',df.connectivity,'Vertices',[df.x df.y],'FaceVertexCData',data(:,it),'FaceColor','interp','EdgeColor','none');
        hold on;
        quiver(df.lon,df.lat,df.u(:,it)*scale,df.v(:,it)*scale,0,'k');
        title([varname ' ' datestr(df.time(it))]);
        hold off;

        if it == 1
            axis tight manual
            set(gca,'nextplot','replacechildren');
            caxis([getfield(variables,varname).min, getfield(variables,varname).max]);
            colorbar;
        end

        writeVideo(v,getframe(gcf));
    end
    close(v);
end


[lonlimit,latlimit]=clp_basemap('lonlim',cl_minmax(df.lon), ...
                'latlim',cl_minmax(df.lat));

varlist = {'temperature'} %'u','v','pressure'};

for iv = 1:length(varlist)

    varname = varlist{iv};
    v = VideoWriter(['schout_' varname '.avi']);
    open(v);
    data = getfield(df, varname);

    for it=1:100:numel(df.time)

        patch('Faces',df.connectivity,'Vertices',[df.x df.y],'FaceVertexCData',squeeze(data(5,:,it)'),'FaceColor','interp','EdgeColor','none');
        title([varname ' ' datestr(df.time(it))]);

        if it == 1
            axis tight manual
            set(gca,'nextplot','replacechildren');
            caxis([getfield(variables,varname).min, getfield(variables,varname).max]);
            colorbar;
        end

        writeVideo(v,getframe(gcf));
    end
    close(v);
end


