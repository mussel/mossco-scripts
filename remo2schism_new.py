import netCDF4
import numpy as np
import glob
import datetime

# from nco 
#import netcdftime

def convert_remo2sfluxes(infile):

  ncin = netCDF4.Dataset(infile, "r")
  # times = ncin.variables['time'][:]

  # for i,time in enumerate(times):
     
  convert_remo2sflux(ncin) #,i

  ncin.close()

def convert_remo2sflux(ncin): #,i

    var = ncin.variables['time']
    #utime=netcdftime.utime(var.units)
    #times=utime.num2date(var[:])
    #days=[times[ti].day for ti in range(0,len(times))]

    days = [int(np.mod(v,100)) for v in var[:]]
    # year, month = int(var[0]//10000), int(np.mod(var[0],10000)//100) # Problem 0 --> Am Ende der Datei neuer Monat
    # print(days, year, month)

    for i, day in enumerate(days[:-1]):
        
        # Warnung: tostring() is depricated. Use tobytes() instead!
        year, month = int(var[i]//10000), int(np.mod(var[i],10000)//100)
        print(days, year, month)
        
        # ncin_sub = ncin(1 day)
        
        base_time = datetime.datetime(year, month, day)
        sflux_name = f"sflux_air_1.{base_time.timetuple().tm_yday:04d}.nc"
        ncout = netCDF4.Dataset(sflux_name, "w")

        create_dimensions(ncin, ncout, base_time)
        create_air(ncin, ncout)
        ncout.close()

        sflux_name = f"sflux_prc_1.{base_time.timetuple().tm_yday:04d}.nc"
        ncout = netCDF4.Dataset(sflux_name, "w")

        create_dimensions(ncin, ncout, base_time)
        create_prc(ncin, ncout)
        ncout.close()

        sflux_name = f"sflux_rad_1.{base_time.timetuple().tm_yday:04d}.nc"
        ncout = netCDF4.Dataset(sflux_name, "w")

        create_dimensions(ncin, ncout, base_time)
        create_rad(ncin, ncout)
        ncout.close()

def specific_humidity(temperature, dewpoint_temperature, pressure):
    # for temperature in Celsius

    gas_water = 461.51
    gas_dryair = 287.058

    humidity_water = saturation_pressure(temperature) \
        /  (gas_water * (temperature + 273.15))
    humidity_dryair = (pressure - saturation_pressure(temperature)) \
        / (gas_dryair * (temperature + 273.15))
    humidity_wetair = humidity_dryair + humidity_water
    return  np.max(humidity_water / humidity_wetair, 1)

def saturation_pressure(temperature):
    # for temperature in Celsius
    # only valid over water not ice

    a = np.where(temperature < 0, 7.6, 7.5)
    b = np.where(temperature >=0, 240.7,  237.3)

    return 6.1078 * 10**((a*temperature)/(b+temperature))

def create_dimensions(ncin, ncout, basetime):

    t = ncin.variables["time"][:]
    # hours = [int(24* (x%1)) for x in ncin.variables["time"][:]]
    
    hours = [ int(24*x%1) for x in t if t%100 == datetime.day]

    ncout.createDimension("lat", ncin.dimensions["rlat"].size)
    ncout.createDimension("lon", ncin.dimensions["rlon"].size)
    ncout.createDimension("time", ncin.dimensions["time"].size) # set unfinite dimension? ODER 24 (h)

    var = ncout.createVariable("time", np.float64, ("time"))
    var.units = f"hours since {basetime.strftime()}" #2isoformat
    var.base_date = np.array([basetime.year, basetime.month, basetime.day, 0], dtype=np.uint16) # Basisdatum setzen

    for varname in ["lat", "lon"]:
        var = ncout.createVariable(varname, 'f4', ("lat", "lon"), fill_value=-9999.0)
        [var.setncattr(att,ncin.variables[varname].getncattr(att)) for att in ncin.variables[varname].ncattrs()]

    # print(ncout.variables["lat"].shape, ncin.variables["lat"])
    ncout.variables["lat"][:] = ncin.variables["lat"][:]
    ncout.variables["lon"][:] = ncin.variables["lon"][:]
    ncout.variables["time"][:] = hours

def create_air(ncin, ncout):

    for varname in ["prmsl", "uwind", "vwind", "stmp", "spfh"]:
        var = ncout.createVariable(varname, 'f4', ("time","lat", "lon"), fill_value=-9999.0)

    [ncout.variables["stmp"].setncattr(att,ncin.variables["var167"].getncattr(att))
        for att in ncin.variables["var167"].ncattrs()]
    ncout.variables["stmp"][:] = ncin.variables["var167"][:]
    ncout.variables["stmp"].units = "degree_C"
    ncout.variables["stmp"].standard_name = "air_temperature_at_2m"
    ncout.variables["stmp"].code = 167

    [ncout.variables["spfh"].setncattr(att,ncin.variables["var168"].getncattr(att))
        for att in ncin.variables["var168"].ncattrs()]
    ncout.variables["spfh"][:] = specific_humidity(ncin.variables["var167"][:],
        ncin.variables["var168"][:], ncin.variables["var151"][:])
    ncout.variables["spfh"].units = "kg kg-1"
    ncout.variables["spfh"].standard_name = "specific_humidity"
    ncout.variables["spfh"].code = "f(167,168,151)"

    [ncout.variables["uwind"].setncattr(att,ncin.variables["var165"].getncattr(att))
        for att in ncin.variables["var165"].ncattrs()]
    ncout.variables["uwind"][:] = ncin.variables["var165"][:]
    [ncout.variables["vwind"].setncattr(att,ncin.variables["var166"].getncattr(att))
        for att in ncin.variables["var166"].ncattrs()]
    ncout.variables["vwind"][:] = ncin.variables["var166"][:]

    [ncout.variables["prmsl"].setncattr(att,ncin.variables["var151"].getncattr(att))
        for att in ncin.variables["var151"].ncattrs()]
    ncout.variables["prmsl"][:] = ncin.variables["var151"][:]
    ncout.variables["prmsl"].standard_name = "surface_air_pressure"
    ncout.variables["prmsl"].units = "Pa"

def create_rad(ncin, ncout):

    var = ncout.createVariable("dswrf", 'f4', ("time","lat", "lon"), fill_value=-9999.0)
    [var.setncattr(att,ncin.variables["var176"].getncattr(att))
        for att in ncin.variables["var176"].ncattrs()]
    var[:] = ncin.variables["var176"][:] - ncin.variables["var204"][:]
    var.units = "W m-2"
    var.standard_name = "downwelling_shortwave_radiation"
    var.code = "176 - 204"

    var = ncout.createVariable("dlwrf", 'f4', ("time","lat", "lon"), fill_value=-9999.0)
    [var.setncattr(att,ncin.variables["var177"].getncattr(att))
        for att in ncin.variables["var177"].ncattrs()]
    var[:] = ncin.variables["var177"][:] - ncin.variables["var205"][:]
    var.units = "W m-2"
    var.standard_name = "downwelling_longwave_radiation"
    var.code = "177 - 205"

def create_prc(ncin, ncout):

    var = ncout.createVariable("prate", 'f4', ("time","lat", "lon"), fill_value=-9999.0)

    [var.setncattr(att,ncin.variables["var142"].getncattr(att))
        for att in ncin.variables["var142"].ncattrs()]
    var[:] = ncin.variables["var142"][:] + ncin.variables["var143"][:]
    var.units = "mm"
    var.standard_name = "precipitation"
    var.code = "142 + 143"

if __name__ == "__main__":

  remo_pattern = "e025166e_196102.nc"
  remo_pattern = "tim_test2.nc"

  for file in glob.glob(remo_pattern):
      convert_remo2sfluxes(file)
