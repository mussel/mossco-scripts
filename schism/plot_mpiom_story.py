#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This script plots interesting aspects of a decadal MPIOM file for the 
display of "stories" about each decade

@author Carsten Lemmen
@license Apache License version 2.0
@copyright 2021 Helmoltz-Zentrum Geesthacht
"""

import pathlib
import numpy as np
import cartopy.crs as ccrs
import cartopy.feature as cfeature
import matplotlib.pyplot as plt
import matplotlib
import netCDF4
import mpiom

vardict = mpiom.vardict

itransect = (140,30)
matplotlib.rcParams['pcolor.shading'] = 'nearest'



def plot_coastal_gradient(x, y, varname):
    
    hax=plt.axes([0.05,0.65,0.3,0.25])
    pcf = hax.plot(x, y,  'k.')

    plt.ylim(vardict.get(varname).get('clim'))
    hax.spines['top'].set_visible(False)
    hax.spines['right'].set_visible(False)
    hax.set_title('Coastal gradient')
    hax.patch.set_visible(True)

    return pcf


def plot_inset_histogram(ax, data):
    
    hax=plt.axes([0.6,0.29,0.35,0.3])
    num, bins, patches = hax.hist(data, bins=50, log=False,
        facecolor='grey', edgecolor=None, alpha=0.6, density = 1, rwidth=1)

    stddev=np.std(data)
    mean=np.mean(data)
    percentiles=np.percentile(data,[5,95])


    plt.setp(hax.axes.yaxis,'ticks_position','right')
    plt.setp(hax.axes.yaxis,'label_position','right')

    plt.setp(hax,'yticklabels',[])
    plt.setp(hax,'yticks',[])
    hax.spines['top'].set_visible(False)
    hax.spines['right'].set_visible(False)
    hax.spines['left'].set_visible(False)
    hax.patch.set_visible(False)

    plt.setp(hax,'xticks',percentiles)
    xtl=hax.get_xticklabels()
    xtl[0]=u'%.1f'%(percentiles[0])
    xtl[1]=u'%.1f'%(percentiles[1])
    hax.set_xticklabels(xtl)

    for i,label in enumerate(hax.get_xticklabels()):
        label.set_fontsize(14)
        label.set_color('grey')

    bax=hax.twinx()
    pos1 = bax.get_position() # get the original position
    bax.set_position([pos1.x0, pos1.y0 - 0.05,  pos1.width, pos1.height]) # set a new position
    bp=bax.boxplot(data, vert=False, showfliers=False)
    bax.text(mean,0.8,u'%.1f\u00B1%.1f'%(mean,stddev),fontsize=14,ha='center')

    for key in bp.keys(): plt.setp(bp[key],'color','k')
    bax.axis('off')

    return


def plot_mpiom_variable(ncid, varname, ax, time_method = np.nanmean, depth_method='top'):
    
    lat = ncid.variables["lat"][:]
    lon = ncid.variables["lon"][:]

    var = ncid.variables[varname]
    dimlist = list(var.dimensions)
    if 'time' in var.dimensions:
        data = time_method(var[:], axis=dimlist.index('time'))
        dimlist.remove('time')
        
    if 'depth' in var.dimensions:
        if depth_method == 'top':
            data = data[0,:,:]
        else:
            data = depth_method(var[:], axis=dimlist.index('depth'))
        
    if varname in vardict:
        pcf = ax.pcolor(lon,lat, data, transform=ccrs.PlateCarree(), 
                  cmap=vardict.get(varname).get('cmap'),
                  vmin = vardict.get(varname).get('clim')[0],
                  vmax = vardict.get(varname).get('clim')[1],
                  )
    else:
        pcf = ax.pcolor(lon,lat, data, transform=ccrs.PlateCarree(), cmap='viridis')
        
    cax=plt.axes([0.1,0.12,0.8,0.05]) 
    cblabel = vardict.get(varname).get('unit')
    cb=plt.colorbar(pcf,cax=cax,orientation='horizontal',label=cblabel)
    
    pih = plot_inset_histogram(ax, data[~data.mask])
    
    pts = ax.plot(lon[itransect[1]:,itransect[0]],lat[itransect[1]:,itransect[0]],'k.')
    pcg = plot_coastal_gradient(lat[itransect[1]:,itransect[0]],data[itransect[1]:, itransect[0]],varname)
    
    #plt.legend(ax,'Hello')
    #vardict.get(varname).get('name'), fontsize=30)

    

def plot_mpiom_decade(filename: str , pltdir: pathlib.Path) -> plt.Figure:
    
    """
    

    Parameters
    ----------
    filename : str
        filename of the MPIOM netcdf
    pltdir : pathlib.Path
        Directory for figure output.

    Returns
    -------
    matoplotlib.pyplot.Figure object

    """    

    ncid = netCDF4.Dataset(filename, "r")
    lat = ncid.variables["lat"][:]
    lon = ncid.variables["lon"][:]

    for i,v in enumerate(["sao", "tho", "zo", "intpp", "ano3", "phosph"]):

        fig = plt.figure(figsize=(10, 8))
        fig.subplots_adjust(left=0.00,right=1,bottom=0.20,top=0.995)
        ax = plot_basemap(fig)

   
        ax.set_extent((np.min(lon), np.max(lon), np.min(lat), np.max(lat)))    

        plot_mpiom_variable(ncid, v, ax)


if __name__ == "__main__":
    
    pattern = '../data/166*'
    
    files = pathlib.Path().glob(pattern)
    
    pltdir = pathlib.Path("figures/")
    pltdir.mkdir(parents=True, exist_ok=True)

    for i, filename in enumerate(files):
        
        plot_mpiom_decade(filename, pltdir)
  

        

