#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 13 12:50:55 2021

@author: lemmen
"""

from schism import schism_setup
import yaml
import numpy as np
import os

if __name__ == "__main__":

    try:
        setup = schism_setup('hgrid.gr3')
    except:
        print("This program needs 'hgrid.gr3'")

    with open('fabm.yaml') as f:
        fabm = yaml.safe_load(f)

    tracers = dict()

    for key, value in fabm['instances'].items():

        with open(f'../fabm/{key}_tracers.yaml') as f:
            print(f'Load tracer specification for {key}')
            tracers[key] = yaml.safe_load(f)[key]

    i=0
    for key, value in fabm['instances'].items():

        for tracer in tracers[key]:
            if not tracer in value['initialization']:
                print(f"Initial file for {key}/{tracer} with 0.0" )
                setup.dump_gr3(f'FBM_hvar_{i+1}.ic',const=0.0, 
                  comment=f'Initialization for {key}/{tracer}')
            else:
                print(f"Initial file for {key}/{tracer} with {value['initialization'][tracer]}" )
                setup.dump_gr3(f'FBM_hvar_{i+1}.ic',const=float(value.get('initialization').get(tracer)), 
                  comment=f'Initialization for {key}/{tracer}')
            i = i+1

