#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar 13 16:54:42 2021

@author: Lemmen
"""

import pathlib
import numpy as np
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
import netCDF4

import mpiom

vardict = mpiom.vardict 


def plot_mpiom_schism_bdy(bdyfilename, ax, cmap='viridis'):
    
    ncid = netCDF4.Dataset(bdyfilename, "r")
    lat = ncid.variables["lat"][:]
    lon = ncid.variables["lon"][:]

    ax.set_extent((np.min(lon)-0.2, np.max(lon)+0.2, np.min(lat)-0.2, np.max(lat)+0.2))

    if "time_series" in ncid.variables:
        data = ncid.variables.get("time_series")
        i = 0 # salt tracer
        varname = 'sao'
    else:
        data = ncid.variables.get("tracer_concentration")
        i = 9 # phosph tracer
        varname = 'phosph'
        

    if len(data.shape) == 4:
        # data.shape should be (108, 2330, 8, 18), i.e. (nt, nb, nz, ntracer)
        data = np.mean(data[:,:,-1,i], axis=0)
    else:
        # data.shape should be (108, 2330, 18), i.e. (nt, nb, ntracer)
        data = np.mean(data[:,:,i], axis=0)

    ax.scatter(lon,lat, c=data, transform=ccrs.PlateCarree(), 
               cmap=vardict.get(varname).get('cmap'),
               vmin=vardict.get(varname).get('clim')[0], 
               #vmax=1, marker='o', 
               vmax=vardict.get(varname).get('clim')[1], marker='o', 
               s=70, linewidth=0.1, edgecolor='k', label=f'Nudge[{i}]')
    ncid.close()


if __name__ == "__main__":
    
    pattern = '../data/166*'
    #pattern = '../data/d*'
    
    files = pathlib.Path().glob(pattern)
    
    for i, filename in enumerate(files):
        
        fig = plt.figure(figsize=(10, 8))
        ax = mpiom.plot_basemap(fig)
  
    
        #plot_mpiom(filename, "sao", ax)
        mpiom.plot_mpiom_variable(netCDF4.Dataset(filename,"r"), "phosph", ax)
      
        bdydir = pathlib.Path("bdy/")
        #bdyfilename = bdydir / filename.name.replace('.nc','bdy_SAL3D.nc')
        bdyfilename = bdydir / filename.name.replace('sns.nc','FBM_maecs_nudge.nc')
        plot_mpiom_schism_bdy(bdyfilename, ax)

        pltdir = pathlib.Path("figures/")
        pltdir.mkdir(parents=True, exist_ok=True)

