#! /bin/bash

yearmonths=$(ls *c151_?????? |cut -d"_" -f3 |sort -u)
#yearmonths=$(ls *c151_196102 |cut -d"_" -f3 |sort -u)

for Y in $yearmonths; do
  ! test -f e025166e_c16X_${Y} && cdo -O merge e025166e_c165_${Y} e025166e_c166_${Y} e025166e_c16X_${Y}
  ! test -f e025166e_c16R_${Y} && cdo -O rotuvb,var165,var166 e025166e_c16X_${Y} e025166e_c16R_${Y}

  for C in 151 142 143 16R 167 168 176 177 204 205; do 
    test -f e025166e_c${C}_${Y}.nc && continue
    cdo -O -f nc copy e025166e_c${C}_${Y}  e025166e_c${C}_${Y}.nc
  done
done

for Y in $yearmonths; do
  test -f e025166e_${Y}.nc && continue
  ncks -O -4 e025166e_c151_${Y}.nc e025166e_${Y}.nc
  for C in 142 143 16R 167 168 176 177 204 205; do 
    ncks -A -4 -C e025166e_c${C}_${Y}.nc e025166e_${Y}.nc
  done
done

