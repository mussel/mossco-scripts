#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 13 12:50:55 2021

@author: lemmen
"""

from schism import schism_setup
import yaml
import numpy as np
import pandas as pd
import os

def main():

    #try:
    #    df_ss = pd.read_csv('source_sink.in', delim_whitespace=True, header=None)
    #except:
    #    print("This program needs 'source_sink.in'")
    #    return

    nsource = 14 #df.iloc[0,0]
    try:
        df = pd.read_csv('msource.th', delim_whitespace=True, header=None)
    except:
        print("This program needs 'msource.th'")
        return

    with open('fabm.yaml') as f:
        fabm = yaml.safe_load(f)

    tracers = dict()

    for key, value in fabm['instances'].items():

        with open(f'../fabm/{key}_tracers.yaml') as f:
            print(f'Load tracer specification for {key}')
            tracers[key] = yaml.safe_load(f)[key]
    i=2

    for key, value in fabm['instances'].items():

        for tracer in tracers[key]:
            if not tracer in value.get('initialization'):
                print(f"Source of {key}/{tracer} is disregarded with value -9999" )
                const = -9999
            else:
                print(f"Source of {key}/{tracer} is constant value {value['initialization'][tracer]}" )
                const = value['initialization'][tracer]

            for j in range(0, nsource): 
               df[str(i) + '_' + str(j)] = const
            i = i+1

    df.to_csv(f'msource_{key}.th', sep=' ', index=False, header=False)

if __name__ == "__main__":
    main()
