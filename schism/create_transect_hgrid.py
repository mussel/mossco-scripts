#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: lemmen, wirtz
"""

import numpy as np
import sys
import pathlib

# Spiekeroog nach Norden 60 km
# Messpfahl ICBM "Otzumer Balje", 53° 45.01644' N und 007°  40.26552' E).

def write_transect_hgrid(directory: pathlib.Path, start, stop):

    # Elements, each a quadrangle split in two triangles
    nelements = 24
    nnodes =  nelements + 2

    # Create nodes on the left (and alternating) on the right side of the
    # transect.
    nodes=np.zeros((nnodes, 4), dtype=float)
    nodes[:,0]=np.array(range( nnodes),dtype=int) + 1
    nodes[0::2,1]=np.linspace(start[0],stop[0], nelements//2+1)-0.01
    nodes[1::2,1]=np.linspace(start[0],stop[0], nelements//2+1)+0.01

    nodes[0::2,2]=np.linspace(start[1],stop[1], nelements//2+1)
    nodes[1::2,2]=np.linspace(start[1],stop[1], nelements//2+1)

    #nodes[0::2,3]= 2+1./(1+np.exp(-(nodes[::2,0]/nelements-0.4)*12))*25+nodes[::2,0]/2
    #nodes[1::2,3]= 2+1./(1+np.exp(-(nodes[::2,0]/nelements-0.4)*12))*25+nodes[::2,0]/2

    # Simple cosine formulation (too thin at coast)
    nodes[0::2,3]=(0.5-0.5*np.cos(nodes[::2,0]/ nelements/2*np.pi))*25.0
    nodes[1::2,3]=(0.5-0.5*np.cos(nodes[::2,0]/ nelements/2*np.pi))*25.0

    #  Create elements as halved quads
    elements=np.zeros((nelements, 5), dtype=int)
    elements[:,0]=np.array(range(nelements),dtype=int) + 1
    elements[:,1]=3

    elements[::2,2]=nodes[0:-2:2,0]
    elements[::2,3]=nodes[1:-2:2,0]
    elements[::2,4]=nodes[2::2,0]

    elements[1::2,2]=nodes[3::2,0]
    elements[1::2,3]=nodes[2::2,0]
    elements[1::2,4]=nodes[1:-2:2,0]

    # Create two land boundaries that connect
    land1=np.zeros((nelements//2+2,1),dtype=int)
    land1[0:nelements//2,0] = elements[::-2,3]
    land1[-2:,0] = elements[0,2:4]

    land2=np.zeros((nelements//2+2,1),dtype=int)
    land2[0:nelements//2,0] = elements[::2,3]
    land2[-2:,0] = elements[-1,2:4]

    with open(directory / 'hgrid.ll', 'w') as f:
        f.write(f'# Transect hgrid created by {sys.argv[0]}\n')
        f.write(f'{nelements} {nnodes} ! number of elements and nodes\n')
        for i,n in enumerate(nodes):
            f.write('%d %.3f %.3f %.2f\n'%(i+1,nodes[i,1],nodes[i,2],nodes[i,3]))
        for i,n in enumerate(elements):
            if (elements[i,1] == 3):
                f.write('%d %d %d %d %d\n'%(elements[i,0], elements[i,1],
                                            elements[i,2], elements[i,3], elements[i,4]))
            elif (elements[i,1] == 4):
                f.write('%d %d %d %d %d %d\n'%(elements[i,0], elements[i,1],
                                               elements[i,2], elements[i,3],
                                               elements[i,4], elements[i,5]))
        f.write('%d # number of ocean boundaries\n'%(0))
        f.write('%d # number of nodes on ocean boundaries\n'%(0))
        f.write('%d # number of land boundaries\n'%(2))
        f.write('%d # number of nodes on land boundaries\n'%(nnodes+2))
        f.write('%d %d # number of land boundaries on land\n'%(nnodes/2+1,0))

        for i,n in enumerate(land1):
            f.write('%d\n'%(land1[i]))
        f.write('%d %d # number of land boundaries on land 2\n'%(nnodes/2+1,0))

        for i,n in enumerate(land2):
            f.write('%d\n'%(land2[i]))

    variables = {'watertype':4, 'windrot_geo2proj':0, 'albedo': 0.01, 'drag': 0.002,
                 'diffmin':0.0, 'diffmax':0.1}
    for key, value in variables.items():
        write_gr3(directory, nodes, nelements, key, value)


def write_gr3(directory, nodes, nelements,  key: str, value):

    with open(directory / (key + '.gr3'), 'w') as f:
        f.write(f'# Transect {key}.gr3 created by {sys.argv[0]}\n')
        f.write(f'{nelements} {len(nodes)} ! number of elements and nodes\n')

        if type(value) == int:
            for i,n in enumerate(nodes):
                f.write('%d %.3f %.3f %d\n'%(i+1,nodes[i,1],nodes[i,2],value))
        else:
            for i,n in enumerate(nodes):
                f.write('%d %.3f %.3f %.3f\n'%(i+1,nodes[i,1],nodes[i,2],value))

if __name__ == '__main__':

    start=(7.66012, 53.738956)
    stop =(7.458982, 54.177829)

    directory = pathlib.Path('/Users/Lemmen/setups/spiekeroog-transect-schism-setup')

    write_transect_hgrid(directory, start, stop)