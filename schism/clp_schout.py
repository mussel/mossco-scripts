#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 16 19:46:04 2021

@author: Lemmen
"""

import pathlib
import sys
import schism
import pickle
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import cmocean
import argparse

import netCDF4


vardict = {
 'albedo': {'cmap': cmocean.cm.gray, 'scale': 100.0},
 'include': {'cmap': cmocean.cm.gray_r, 'type':'int', },
 'diffmin':  {'cmap': cmocean.cm.matter},
 'TEM_nudge': {'cmap': cmocean.cm.gray_r, 'scale': 8640000.0,   'vlim': (0,100)},
 'watertype': {'cmap': cmocean.cm.phase,  'type': 'int', 'vlim': [1, 7]},
 'windrot_geo2proj': {'cmap': cmocean.cm.phase},
 'diffmax': {'cmap': cmocean.cm.tarn},
 'depths': {'cmap': cmocean.cm.deep},
 'depth': {'cmap': cmocean.cm.deep},
 'uvel': {'cmap': cmocean.cm.curl,'vlim': [-1, 1], 'units': 'm s-1'},
 'vvel': {'cmap': cmocean.cm.curl,'vlim': [-1, 1], 'units': 'm s-1'},
 'maecs_Chl': {'cmap': cmocean.cm.algae},
 'maecs_phyC': {'cmap': cmocean.cm.algae},
 'elev' : {'cmap': cmocean.cm.curl,'vlim': [-3, 3], 'units': 'm'},
 'air_pressure' : {'cmap': cmocean.cm.curl, 'units': 'Pa'},
}

def plot_node_data(setup, nc):

    for varid in nc.variables:

        if not varid in ['air_pressure']: continue

        plot_node_variable(setup, nc[varid], nc)


def plot_node_variable(setup, varid, nc):

    dims = varid.dimensions
    item = varid.name

    x = nc['SCHISM_hgrid_node_lon'][:] if 'SCHISM_hgrid_node_lon' in nc.variables else nc['SCHISM_hgrid_node_x'][:]
    y = nc['SCHISM_hgrid_node_lat'][:] if 'SCHISM_hgrid_node_lat' in nc.variables else nc['SCHISM_hgrid_node_y'][:]
    #triangles = nc['SCHISM_hgrid_face_nodes'][:,:3]

    if not 'nSCHISM_hgrid_node' in dims: return

    if 'time' in dims:
        time = nc.variables['time'][:] if 'time' in nc.variables else list(range(nc.dimensions['time'].size))

    cmap = vardict.get(item).get('cmap') if item in vardict and 'cmap' in vardict.get(item) else 'viridis'
    unit = vardict.get(item).get('unit') if item in vardict and  'unit' in vardict.get(item) else ''

    if len(dims) == 1:
        d = varid[:]
    elif len(dims) == 2 and dims[1] == 'nSCHISM_vgrid_layers':
        d = varid[:,-1] # surface layer
    elif len(dims) == 2 and dims[0] == 'time':
        d = varid[:,:]
    elif len(dims) == 3:
        if dims[0] == 'time': 
            if dims[-1] == 'two': 
                d = np.sqrt( varid[:,:,0]**2 + varid[:,:,1]**2)

    (vmin, vmax) = vardict.get(item).get('vlim') if item in vardict and  'vlim' in vardict.get(item) else (np.min(d), np.max(d))

    for it, t in enumerate(time):

        if np.mod(it,6) > 0: continue

        if dims[0] == 'time':
            if len(dims) == 2:
                d = varid[it,:]
            elif len(dims) == 3:
                d = varid[it,:,-1]

        fig, ax = plt.subplots(1,1)

        plt.tripcolor(x, y, setup.triangles, d,
                  cmap=cmap, vmin=vmin, vmax=vmax)

        cb = plt.colorbar()
        cb.set_label(item + ' (' + unit + ')')
        #psetup.plot_inset_histogram(ax, d)

        #plt.savefig(f"schism_output_{item}.pdf")
        filename = f"schism_output_surface_{item}_{t}.png"
        print(f"Saving to {filename} ...")
        plt.savefig(filename, dpi=300)

        plt.close()

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument("-r", "--repickle", help="do not use a possibly existing pickle",
                    action="store_true", default=False)
    parser.add_argument("-s", "--setup", help="path to setup directory containing hgrid.ll",
                    type=pathlib.Path, default=pathlib.Path('.'), nargs='?')    
    parser.add_argument("-f", "--files", help="pattern for matching the files to plot",
        default=pathlib.Path('./outputs/').glob('schout_?.nc'), type=pathlib.Path.glob, nargs='+')    
    parser.add_argument("-v", "--variables", help="comma-separated list of variable names to plot",
        default='air_pressure', type=str, nargs='?')    
    args = parser.parse_args()

    setup_dir = args.setup

    hgrid_pickle = setup_dir / 'hgrid.pickle'
    hgrid_ll = setup_dir / 'hgrid.ll'
    hgrid_gr3 = setup_dir / 'hgrid.gr3'
    vgrid_in = setup_dir / 'vgrid.in'

    if hgrid_pickle.exists() and not args.repickle:
        with open(hgrid_pickle, 'rb') as f:
            print(f'Loading from {hgrid_pickle}')
            setup = pickle.load(f)
    else:
        import schism
        setup = schism.schism_setup(hgrid_gr3 , ll_file = hgrid_ll, vgrid_file=vgrid_in)
        if not hasattr(setup, '.triangles'):
          setup.triangles=np.array(setup.nv) - 1
        with open(hgrid_pickle,'wb') as f:
          pickle.dump(setup,f)
    
    variables = args.variables.split(',')

    for output_file in args.files: 
        nc = netCDF4.Dataset(output_file, 'r')

        for varid in nc.variables:
            if not varid in variables: continue
            plot_node_variable(setup, nc[varid], nc)
    return setup

if __name__ == "__main__":
    setup = main()
