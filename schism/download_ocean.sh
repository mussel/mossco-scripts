#! /in/bash

if [ "$1" == "" ]; then 
exit 1
fi

T=$1

test -f 166_mpiom_data_2d_${T}_mavrt.nc || test -f 166_mpiom_data_2d_${T}_mavrt.nc.gz || jblob -d DKRZ_LTA_845_ds00001 --origin "race/output/NASH06_C20r1/166/${T}/166_mpiom_data_2d_${T}_mavrt.nc.gz"
test -f 166_mpiom_data_2d_${T}_mavrt.nc || gunzip 166_mpiom_data_2d_${T}_mavrt.nc.gz
 
# Monthly 3D files for salinity, temperature, u, v
for C in sao tho uko vke; do
  test -f 166_mpiom_data_3d_${T}_mavrt_${C}.nc || test -f 166_mpiom_data_3d_${T}_mavrt_${C}.nc.gz || jblob -d DKRZ_LTA_845_ds00001 --origin "race/output/NASH06_C20r1/166/${T}/166_mpiom_data_3d_${T}_mavrt_${C}.nc.gz"
  test -f 166_mpiom_data_3d_${T}_mavrt_${C}.nc || gunzip 166_mpiom_data_3d_${T}_mavrt_${C}.nc.gz
done


for C in ano3 det graz oxygen phosph phosy phy silt zoo; do
  test -f 166_hamocc_data_3d_${T}_mavrt_${C}.nc || test -f 166_hamocc_data_3d_${T}_mavrt_${C}.nc.gz || jblob -d DKRZ_LTA_845_ds00001 --origin "race/output/NASH06_C20r1/166/${T}/166_hamocc_data_3d_${T}_mavrt_${C}.nc.gz"
  test -f 166_hamocc_data_3d_${T}_mavrt_${C}.nc || gunzip 166_hamocc_data_3d_${T}_mavrt_${C}.nc.gz
done

G=nash06_mpiom_fx.nc

test -f 166_mpiom_data_3d_${T}_mavrt_uvrot.nc || cdo mrotuvb -setgrid,$G -sethalo,-1,-1 -selvar,uko 166_mpiom_data_3d_${T}_mavrt_uko.nc -setgrid,$G -sethalo,-1,-1 -selvar,vke 166_mpiom_data_3d_${T}_mavrt_vke.nc 166_mpiom_data_3d_${T}_mavrt_uvrot.nc

test -f  166_mpiom_data_3d_${T}_mavrt_uv.nc || cdo setctomiss,0 -setgrid,$G 166_mpiom_data_3d_${T}_mavrt_uvrot.nc 166_mpiom_data_3d_${T}_mavrt_uv.nc

if ! test -f 166_mpiom_data_3d_${T}_mavrt_sns.nc ; then 
  ncks -O -d y,329,415,1 -d x,247,411,1 -d depth_2,0,7,1 166_mpiom_data_3d_${T}_mavrt_uv.nc 166_mpiom_data_3d_${T}_mavrt_sns.nc
  ncrename -d .depth_2,depth 166_mpiom_data_3d_${T}_mavrt_sns.nc
  ncks -A -d y,329,415,1 -d x,247,411,1 -d depth,0,7,1 -C -v sao 166_mpiom_data_3d_${T}_mavrt_sao.nc 166_mpiom_data_3d_${T}_mavrt_sns.nc
  ncks -A -d y,329,415,1 -d x,247,411,1 -d depth,0,7,1 -C -v tho 166_mpiom_data_3d_${T}_mavrt_tho.nc 166_mpiom_data_3d_${T}_mavrt_sns.nc
  ncwa -O -a depth -d y,329,415,1 -d x,247,411,1 -C -v zmld 166_mpiom_data_2d_${T}_mavrt.nc zmld_${T}.nc
  ncks -A -C -v zmld zmld_${T}.nc 166_mpiom_data_3d_${T}_mavrt_sns.nc

  for C in graz phosy; do
    ncrename -d .depth_2,depth 166_hamocc_data_3d_${T}_mavrt_$C.nc
  done
 
  for C in ano3 det graz oxygen phosph phosy phy silt zoo; do
    ncks -A -d y,329,415,1 -d x,247,411,1 -d depth,0,7,1 -C -v $C 166_hamocc_data_3d_${T}_mavrt_$C.nc 166_mpiom_data_3d_${T}_mavrt_sns.nc
  done
fi
