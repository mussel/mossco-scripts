#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 13 12:50:55 2021

@author: lemmen
"""

from schism import schism_setup
import numpy as np
import netCDF4
import os

def write_bdy_ugrid(setup,filename,znum=1,ncom=1):
    """
    write boundary ugrid file
    """
     
    if setup.num_bdy_nodes==0:
      print('  setup has no open boundaries')
      return
     
    nbdndes = setup.num_bdy_nodes
     
    nc = netCDF4.Dataset(filename,'w',format='NETCDF4_CLASSIC')
    nc.createDimension('time',None)
    nc.createDimension('nOpenBndNodes',nbdndes)
    nc.createDimension('nLevels',znum)
    nc.createDimension('nComponents',ncom)
    nc.createDimension('one',1)
 
    v = nc.createVariable('time_step','f4',('one'))
    v.long_name = 'time step'
    v.units = 'seconds'
    v[:] = 3600.
 
    v = nc.createVariable('time','f8',('time'))
    v.long_name = 'simulation time in seconds'
    v.units = 'days since start'
    v[:] = 0.0
     
    v = nc.createVariable('lon','f8',('nOpenBndNodes'))
    v[:] = setup.get_bdy_latlon()[0]
    v.standard_name = "longitude"
    v.units = "degrees_east" 
                                      
    v = nc.createVariable('lat','f8',('nOpenBndNodes'))
    v[:] = setup.get_bdy_latlon()[1]
    v.standard_name = "latitude"
    v.units = "degrees_north"

    v = nc.createVariable('locstream','i4',('one'))
    v.cf_role = "mesh_topology"
    v.topology_dimension = 1
    v.node_coordinates = "lon lat"
    v.face_coordinates = "lon lat"

    v = nc.createVariable('time_series','f4',('time', 'nOpenBndNodes', 'nLevels', 'nComponents'))
    v.location = "node"

    nc.Convention = "CF-1.7, UGRID-1.0"
      
    nc.close()
    return

if __name__ == "__main__":

    try:
        setup = schism_setup('hgrid.gr3', ll_file='hgrid.ll', vgrid_file='vgrid.in')
    except:
        print("This program needs 'hgrid.gr3, hgrid.ll and vgrid.in'")


    write_bdy_ugrid(setup, 'testbdy.nc')
