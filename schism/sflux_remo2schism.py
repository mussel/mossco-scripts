import netCDF4
import numpy as np
import glob
import datetime
import sys

def convert_remo2sfluxes(monthfile):

  ncin = netCDF4.Dataset(monthfile, "r")
  var = ncin.variables['time']

  # Extract the day information, disregard last time point, as
  # this is the beginning of the next month
  days = np.unique([int(np.mod(v,100)) for v in var[:-1]])
  year, month = int(var[0]//10000), int(np.mod(var[0],10000)//100)

  for i,day in enumerate(days):
      #print(i,year, month, day)
      basetime = datetime.datetime(year, month, day, 0, 0, 0) #CHANGE!!!
      convert_remo2sflux(ncin, basetime)


  ncin.close()

def convert_remo2sflux(ncin, basetime):

    sflux_name = f"sflux_air_1.{basetime.timetuple().tm_yday:04d}.nc"
    ncout = netCDF4.Dataset(sflux_name, "w")
    create_dimensions(ncin, ncout, basetime)
    create_air(ncin, ncout, basetime)
    ncout.close()

    sflux_name = f"sflux_prc_1.{basetime.timetuple().tm_yday:04d}.nc"
    ncout = netCDF4.Dataset(sflux_name, "w")
    create_dimensions(ncin, ncout, basetime)
    create_prc(ncin, ncout, basetime)
    ncout.close()

    sflux_name = f"sflux_rad_1.{basetime.timetuple().tm_yday:04d}.nc"
    ncout = netCDF4.Dataset(sflux_name, "w")
    create_dimensions(ncin, ncout, basetime)
    create_rad(ncin, ncout, basetime)
    ncout.close()

def specific_humidity(temperature, dewpoint_temperature, pressure):
    # for temperature in Kelvin, pressure in Pa
    # temp_cel = temperature - 273.15 # temp_cel to calculate vapor_pressure
    dewpoint_celsius = dewpoint_temperature - 273.15
    gas_water = 461.51
    gas_dryair = 287.058

    # calculation: s=p(water)/p(wetair) --> p(wetair)=p(dryair)+p(water)
    # p(water) = e/Rw*T ; p(dryair) = e/Rd*T (T in K, e = water vapor pressure)
    # Source: https://de.wikipedia.org/wiki/Luftfeuchtigkeit#Spezifische_Luftfeuchtigkeit
    # Source: https://www.schweizer-fn.de/lueftung/feuchte/feuchte.php
    # results in kg water/kg air

    density_water = saturation_vapor_pressure(dewpoint_celsius) \
        /  (gas_water * temperature)
    density_dryair = (pressure - saturation_vapor_pressure(dewpoint_celsius)) \
        / (gas_dryair * temperature)
    density_wetair = density_dryair + density_water
    return  np.max(density_water / density_wetair, 1)

def saturation_vapor_pressure(temperature, formula='Huang'):
    """
    Calculate the saturation vapor pressure over ice and water according
    to the new formulation by Huang (2018, https://doi.org/10.1175/JAMC-D-17-0334.1)
    or the improved Magnus formula.

    Requires temperature in degree Celsius as input and returns saturation
    vapor pressure in Pa
    """

    magnus_wa, magnus_wb, magnus_wc = [17.625, 243.04, 610.94]
    magnus_ia, magnus_ib, magnus_ic = [22.587, 273.86, 611.21]

    huang_wa, huang_wb, huang_wc, huang_wd1, huang_wd2 = [
      34.494, 4924.99, 1.57, 237.1, 105.0]
    huang_ia, huang_ib, huang_ic, huang_id1, huang_id2 = [
      43.494, 6545.8, 2.0, 278.0, 868]

    if formula == 'Magnus':
        svp = np.where(temperature > 0.0,
            magnus_wc * np.exp((magnus_wa*temperature)/(magnus_wb+temperature)),
            magnus_ic * np.exp((magnus_ia*temperature)/(magnus_ib+temperature)),
            )
    else: # default formula is Huang 2018
        svp = np.where(temperature > 0.0,
          np.exp(huang_wa - huang_wb / (temperature + huang_wd1)) /
            (temperature + huang_wd2)**huang_wc,
          np.exp(huang_ia - huang_ib / (temperature + huang_id1)) /
            (temperature + huang_id2)**huang_ic)

    return svp

def test_saturation_vapor_pressure():
    """
    Unit test for calculation of saturation vapor pressure
    using Table 1 and Table 2 from Huang 2018
    """

    temperature = np.array(range(-100,130,20),dtype=float)
    temperature[-1] = 0.01
    temperature=np.sort(temperature)
    pressure_huang = [0.001405,0.05477, 1.0814,12.841, 103.23,611.29,
        611.689,2339.32,7384.93,19946.1, 47415.0, 101417]

    assert (pressure_huang - saturation_vapor_pressure(temperature)) / pressure_huang < 0.01, "Saturation vapor pressure calculation"

def create_dimensions(ncin, ncout, basetime):

    var = ncin.variables["time"][:] #explenation
    # each datapoint represents the mean of the previous hour
    # therefore datapoints are shifted forward by half an hour

    # fractionaldays = sorted([(x-1/48.)%1 for x in var[:] if np.int((x-1/48.)%100) == basetime.day])
    fractionaldays = sorted([(x-1/48.)%1 for x in var[:] if np.int(x%100) == basetime.day])

    ncout.createDimension("lat", ncin.dimensions["rlat"].size)
    ncout.createDimension("lon", ncin.dimensions["rlon"].size)
    ncout.createDimension("time") # record dimension time

    var = ncout.createVariable("time", np.float64, ("time"))
    var.units = "days since " + basetime.isoformat() #.strftime("%B %d, %Y") #2isoformat
    var.axis = "T"
    var.base_date = np.array([basetime.year, basetime.month, basetime.day, 0], dtype=np.uint16)
    #var.base_date = np.array([2012, basetime.month, basetime.day, 0], dtype=np.uint16)

    for varname in ["lat", "lon"]:
        var = ncout.createVariable(varname, 'f4', ("lat", "lon"), fill_value=-9999.0)
        [var.setncattr(att,ncin.variables[varname].getncattr(att)) for att in ncin.variables[varname].ncattrs()]

    ncout.variables["lat"][:] = ncin.variables["lat"][:]
    ncout.variables["lon"][:] = ncin.variables["lon"][:]
    ncout.variables["time"][:] = fractionaldays

def create_air(ncin, ncout, basetime):

    var = ncin.variables["time"][:] #CHANGE!!!
    itime = [i for i,x in enumerate(var[:]) if np.int((x-1/48.)%100) == basetime.day]
    # last timepoint (19610201.00) is not shifted into previous month (19610201-1/48>196101!!)
    # this hack append the last day anyway
    if itime[-1] == len(var) - 2:
        itime.append(len(var) - 1)

    for varname in ["prmsl", "uwind", "vwind", "stmp", "spfh"]:
        var = ncout.createVariable(varname, 'f4', ("time","lat", "lon"), fill_value=-9999.0)

    [ncout.variables["stmp"].setncattr(att,ncin.variables["var167"].getncattr(att))
        for att in ncin.variables["var167"].ncattrs()]
    ncout.variables["stmp"][:] = ncin.variables["var167"][itime,:,:]
    ncout.variables["stmp"].units = "K" 
    ncout.variables["stmp"].standard_name = "air_temperature_at_2m"
    ncout.variables["stmp"].code = 167

    [ncout.variables["spfh"].setncattr(att,ncin.variables["var168"].getncattr(att))
        for att in ncin.variables["var168"].ncattrs()]
    ncout.variables["spfh"][:] = specific_humidity(ncin.variables["var167"][itime,:,:],
        ncin.variables["var168"][itime,:,:], ncin.variables["var151"][itime,:,:])
    ncout.variables["spfh"].units = "kg kg-1"
    ncout.variables["spfh"].standard_name = "specific_humidity"
    ncout.variables["spfh"].code = "f(167,168,151)"

    [ncout.variables["uwind"].setncattr(att,ncin.variables["var165"].getncattr(att))
        for att in ncin.variables["var165"].ncattrs()]
    ncout.variables["uwind"][:] = ncin.variables["var165"][itime,:,:]
    [ncout.variables["vwind"].setncattr(att,ncin.variables["var166"].getncattr(att))
        for att in ncin.variables["var166"].ncattrs()]
    ncout.variables["vwind"][:] = ncin.variables["var166"][itime,:,:]

    [ncout.variables["prmsl"].setncattr(att,ncin.variables["var151"].getncattr(att))
        for att in ncin.variables["var151"].ncattrs()]
    ncout.variables["prmsl"][:] = ncin.variables["var151"][itime,:,:]
    ncout.variables["prmsl"].standard_name = "surface_air_pressure"
    ncout.variables["prmsl"].units = "Pa"

def create_rad(ncin, ncout, basetime):

    var = ncin.variables["time"][:] #CHANGE!!!
    itime = [i for i,x in enumerate(var[:]) if np.int((x-1/48.)%100) == basetime.day]
    # last timepoint (19610201.00) is not shifted into previous month (19610201-1/48>196101!!)
    # this hack append the last day anyway
    if itime[-1] == len(var) - 2:
        itime.append(len(var) - 1)

    var = ncout.createVariable("dswrf", 'f4', ("time","lat", "lon"), fill_value=-9999.0)
    [var.setncattr(att,ncin.variables["var176"].getncattr(att))
        for att in ncin.variables["var176"].ncattrs()]
    var[:] = ncin.variables["var176"][itime,:,:] - ncin.variables["var204"][itime,:,:]
    var.units = "W m-2"
    var.standard_name = "downwelling_shortwave_radiation"
    var.code = "176 - 204"

    var = ncout.createVariable("dlwrf", 'f4', ("time","lat", "lon"), fill_value=-9999.0)
    [var.setncattr(att,ncin.variables["var177"].getncattr(att))
        for att in ncin.variables["var177"].ncattrs()]
    var[:] = ncin.variables["var177"][itime,:,:] - ncin.variables["var205"][itime,:,:]
    var.units = "W m-2"
    var.standard_name = "downwelling_longwave_radiation"
    var.code = "177 - 205"

def create_prc(ncin, ncout, basetime):

    var = ncin.variables["time"][:] #CHANGE!!!
    itime = [i for i,x in enumerate(var[:]) if np.int((x-1/48.)%100) == basetime.day]
    # last timepoint (19610201.00) is not shifted into previous month (19610201-1/48>196101!!)
    # this hack append the last day anyway
    if itime[-1] == len(var) - 2:
        itime.append(len(var) - 1)

    var = ncout.createVariable("prate", 'f4', ("time","lat", "lon"), fill_value=-9999.0)

    [var.setncattr(att,ncin.variables["var142"].getncattr(att))
        for att in ncin.variables["var142"].ncattrs()]
    var[:] = ncin.variables["var142"][itime,:,:] + ncin.variables["var143"][itime,:,:]
    var.units = "mm h-1"
    var.standard_name = "precipitation"
    var.code = "142 + 143"

if __name__ == "__main__":

  # remo_pattern = "e025166e_sns_196101.nc"
  # remo_pattern = "e025166e_sns_1961*.nc"

  inFile = sys.argv[1]

  remo_pattern = inFile

  for monthfile in glob.glob(remo_pattern):
      convert_remo2sfluxes(monthfile)
