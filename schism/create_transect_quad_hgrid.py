#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: lemmen
"""

import numpy as np
import csv

# Spiekeroog nach Norden 60 km
# Messpfahl ICBM "Otzumer Balje", 53° 45.01644' N und 007°  40.26552' E).

def write_transect_hgrid(filename, start, stop):

    #  elements, each a quadrangle
    nelements = 10
    nnodes = 2*nelements + 2


    nodes=np.zeros((nnodes, 4), dtype=float)
    nodes[:,0]=np.array(range(nnodes),dtype=int) + 1
    nodes[0::2,1]=np.linspace(start[0]-0.1,stop[0],nelements+1)
    nodes[1::2,1]=np.linspace(start[0]+0.1,stop[0],nelements+1)

    nodes[0::2,2]=np.linspace(start[1],stop[1],nelements+1)
    nodes[1::2,2]=np.linspace(start[1],stop[1],nelements+1)


    nodes[0::2,3]=(0.5-0.5*np.cos(nodes[::2,0]/nelements/2*np.pi))*25.0
    nodes[1::2,3]=(0.5-0.5*np.cos(nodes[::2,0]/nelements/2*np.pi))*25.0

    elements=np.zeros((nelements, 6), dtype=int)

    elements[:,0]=np.array(range(nelements),dtype=int) + 1
    elements[:,1]=4
    elements[:,2]=nodes[:-2:2,0]
    elements[:,3]=nodes[1:-2:2,0]
    elements[:,4]=nodes[3::2,0]
    elements[:,5]=nodes[2::2,0]

    land1=np.zeros((nelements+2,1),dtype=int)

    land1[0] = elements[-1,-1]
    land1[1:nelements+1,0] = elements[::-1,2]
    land1[nelements+1,0] = 2

    land2=np.zeros((nelements+2,1),dtype=int)

    land2[0] = 2
    land2[1:nelements+1,0] = elements[::,4]
    land2[nelements+1,0] = nnodes-1


    print('! transect hgrid created by create_transect_hgrid.py')
    print('%d %d ! number of elements and nodes'%(nelements,nnodes))

    for i,n in enumerate(nodes):
        print('%d %f %f %f'%(i+1,nodes[i,1],nodes[i,2],nodes[i,3]))

    for i,n in enumerate(elements):
        print('%d %d %d %d %d %d'%(elements[i,0], elements[i,1], elements[i,2], elements[i,3], elements[i,4], elements[i,5],))

    # land boundaries
    print('%d # number of ocean boundaries'%(0))
    print('%d # number of nodes on ocean boundaries'%(0))
    print('%d # number of land boundaries'%(2))
    print('%d # number of nodes on land boundaries'%(nnodes+2))
    print('%d %d # number of land boundaries on land 1'%(nnodes/2+1,0))

    for i,n in enumerate(land1):
        print('%d'%(land1[i]))

    print('%d %d # number of land boundaries on land 2'%(nnodes/2+1,0))


    for i,n in enumerate(land2):
        print('%d'%(land2[i]))


    # Drag.gr3 and others
    for i,n in enumerate(nodes):
        print('%d %f %f %f'%(i+1,nodes[i,1],nodes[i,2],0.002))


if __name__ == '__main__':

    start=(7.66012, 53.738956)
    stop =(7.458982, 54.177829)

    write_transect_hgrid('hgrid.ll', start, stop)