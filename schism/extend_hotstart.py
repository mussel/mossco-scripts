import netCDF4
import numpy as np


def extend_hotstart(hotstart_in, hotstart_out, ntracers):

  ncin = netCDF4.Dataset(hotstart_in, "r")
  ncout = netCDF4.Dataset(hotstart_out, "w", format='NETCDF4')

  for key,value in ncin.dimensions.items():


      if value.name == 'ntracers':
          ncout.createDimension(value.name,ntracers)
      else:
          ncout.createDimension(value.name,value.size)

  for key,value in ncin.variables.items():

      try:
          var=ncout.createVariable(key,value.dtype,value.dimensions,fill_value=value.getncattr('_FillValue'))
      except:
          var=ncout.createVariable(key,value.dtype,value.dimensions)

      for att in value.ncattrs():
          if att == '_FillValue': continue
          var.setncattr(att,value.getncattr(att))

  for key,value in ncin.variables.items():

      if not key in ncout.variables.keys(): continue

      if 'ntracers' in value.dimensions:
          dimlen = ncin.dimensions['ntracers'].size
          ncout.variables[key][:,:,:dimlen]=ncin.variables[key][:,:,:dimlen]
          ncout.variables[key][:,:,dimlen:]=0.0
      else:
          ncout.variables[key][:]=ncin.variables[key][:]

  ncout.close()
  ncin.close()

if __name__ == "__main__":

  hotstart_in = "hotstart.nc"
  hotstart_out = "hotstart_out.nc"
  ntracers = 20

  extend_hotstart(hotstart_in, hotstart_out, ntracers)
