#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 16 19:46:04 2021

@author: Lemmen
"""

import pathlib
import sys
import schism
import pickle
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

import plot_schism_setup

# !       Read in water type; the values for R, d_1, d_2 are given below
# !       solar flux= R*exp(z/d_1))+(1-R)*exp(z/d_2) (d_[1,2] are attentuation depths; smaller values for muddier water)
# !       1: 0.58 0.35 23 (Jerlov type I)
# !       2: 0.62 0.60 20 (Jerlov type IA)
# !       3: 0.67 1.00 17 (Jerlov type IB)
# !       4: 0.77 1.50 14 (Jerlov type II)
# !       5: 0.78 1.40 7.9 (Jerlov type III)
# !       6: 0.62 1.50 20 (Paulson and Simpson 1977; similar to type IA)
# !       7: 0.80 0.90 2.1 (Mike Z.'s choice for estuary)

# The following need to be added to schism_step.F90
#         8: 0.80 1.10 4.0 # interpolation between 4 and 9
#         9: 0.80 0.70 1.6 # very turbid water in North Sea Coast

def main():

    #setup_dir = '.'
    setup_dir = "/Users/Lemmen/temp"
    #setup_dir = "/Users/Lemmen/devel/schism/schism_verification_tests/Test_CORIE"
    #setup_dir = "/Users/Lemmen/devel/schism/schism_verification_tests/Test_HeatConsv_TVD"

    if len(sys.argv)>1:
        setup_dir = sys.argv[0]

    hgrid_pickle = pathlib.Path(setup_dir) / 'hgrid.pickle'
    hgrid_gr3 = pathlib.Path(setup_dir) / 'hgrid.gr3'

    if hgrid_pickle.exists():
        with open(hgrid_pickle, 'rb') as f:
            setup = pickle.load(f)
    else:
        setup = schism.schism_setup(hgrid_gr3 ,
            ll_file = pathlib.Path(setup_dir) / 'hgrid.ll',
            vgrid_file=pathlib.Path(setup_dir) / 'vgrid.in'
        )

    # Default is Jerlov type II
    setup.watertype = np.ones((setup.nnodes),dtype=int) * 4
    mask = (np.array(setup.depths) <= 12)
    setup.watertype[mask] = 7


    mask = (np.array(setup.x) < -100000) * (np.array(setup.y) < 5800000)
    setup.watertype[mask] = 4
    setup.watertype = list(setup.watertype)
    setup.dump_gr3_spat_var('watertype_12m_northwest.gr3', setup.watertype, comment='Created by create_watertype_scenarios.py')

    # Default is Jerlov type II
    # Apply 8 to everywhere < 20 m
    # Apply 9 to everywhere < 12 m
    # then correct British coastline back to 8 (apply 9 to east coast North Sea.)
    # and northern part of british coast back to 4
    # (not yet) Apply 3 in open North Sea with warm bias areas
    setup.watertype = np.ones((setup.nnodes),dtype=int) * 4
    mask = (np.array(setup.depths) <= 20)
    setup.watertype[mask] = 8
    mask = (np.array(setup.depths) <= 12)
    setup.watertype[mask] = 9

    #mask = (np.array(setup.x) < -100000) * (np.array(setup.y) < 5800000)
    mask = (np.array(setup.x) < -100000) * (setup.watertype > 7)
    setup.watertype[mask] = 8


    mask = (np.array(setup.lon) < 6) * (np.array(setup.lon) > 1 ) *  (np.array(setup.lat) > 54)  * (setup.watertype > 4)
    setup.watertype[mask] = 4


    setup.watertype = list(setup.watertype)
    setup.dump_gr3_spat_var('watertype_20m_12m_west.gr3', setup.watertype, comment='Created by create_watertype_scenarios.py')



    return setup

if __name__ == "__main__":
    setup = main()