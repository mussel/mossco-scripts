import netCDF4
import numpy as np
import datetime
import netcdftime
import pathlib
import matplotlib.pyplot as plt

plt.style.use('seaborn')


def read_netcdf_times(files: pathlib.Path.glob) -> list[datetime.datetime]:

    time = []
    for f in files:
        ncid = netCDF4.Dataset(f, "r")
        var = ncid.variables['time']
        utime = netcdftime.utime(var.units)
        mytime = utime.num2date(list(var[:]))
        time.extend(mytime)
        print(f'{f} {np.min(mytime)} {np.min(time)}')
        ncid.close()
    return time

def read_netcdf_varnames(filename: str) -> list[str]:

    ncid = netCDF4.Dataset(filename, "r")
    varnames = []
    for varname, var in ncid.variables.items():

        dims = var.dimensions

        if not 'time' in dims != 3: continue
        if not 'lon' in dims != 3: continue
        if not 'lat' in dims != 3: continue

        varnames.append(varname)
    ncid.close()
    return varnames

def read_netcdf_variable(files: pathlib.Path.glob, varname):

    value = []
    for f in files:
        ncid = netCDF4.Dataset(f, "r")

        var = ncid.variables[varname]
        unit = var.units if hasattr(var,'units') else ' '
        dims = var.dimensions

        average = set(range(len(dims)))
        average.remove(var.dimensions.index('time'))

        var.dimensions.index('time')

        value.extend(list(np.mean(var[:], axis=tuple(average))))

        ncid.close()
    return value, unit

def clp_timeseries(time, value, units, varname):

    uvalue = np.unique(np.array(value))
    if len(uvalue) == 1:
        print(f"Variable {varname} is constant {uvalue}")
        return

    fig, ax = plt.subplots(nrows=1, ncols=1)
    ax.plot_date(time, value)
    ax.set_ylabel(f'{varname} / {units}')
    plt.tight_layout()
    plt.gcf().autofmt_xdate
    plt.savefig(f'sflux_{varname}.png', dpi=300)


def clp_sflux_timeseries(path: pathlib.Path):

    for domain in ('air', 'prc', 'rad'):
        files = sorted(list(path.glob('sflux_' + domain + '_1.*.nc')))
        if len(files) < 1: continue

        time = read_netcdf_times(files)
        varnames = read_netcdf_varnames(files[0])

        for varname in varnames:
            value, unit = read_netcdf_variable(files, varname)
            clp_timeseries(time, value, unit, f'{domain}-{varname}')


if __name__ == "__main__":

    sflux_path=pathlib.Path('/Users/Lemmen/setups/helgoland-schism-setup/sflux')

    clp_sflux_timeseries(sflux_path)