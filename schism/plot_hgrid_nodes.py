#!/usr/bin/env python

import numpy as np
from schism import schism_setup
import matplotlib.pyplot as plt

if __name__ == '__main__':

    setup = schism_setup('hgrid.gr3',ll_file='hgrid.gr3', vgrid_file='vgrid.in')
    # plot domain

    plt.triplot(setup.x,setup.y,np.asarray(setup.nv)-1, color = 'y')
    plt.text(setup.x,setup.y, [str(i) for i in list(np.arange(len(setup.x)))], fontsize=1)
    plt.savefig('hgrid_nodes.pdf')

