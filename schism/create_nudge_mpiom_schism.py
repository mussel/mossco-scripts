#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@copyright 2021 Helmholtz-Zentrum Geesthacht
@author: Carsten Lemmen <carsten.lemmen@hzg.de>
@author: Tim Mauch
@license: GNU Public License v3.0
"""

import netCDF4
import sys
import pathlib
import schism
import pickle
import numpy as np
import re
import netcdftime
import yaml


# map maecs names to hamocc names
hamoccmaecs = {"nutP": "phosph", "nutN": "ano3", "phyC": "phy",
             "detC": "det", "oxy": "oxygen", "zooC": "zoo"}

def write_nudge_netcdf(setup, filename, time, data, frcbdnodes):
      """
      write boundary data for schism setup
      """

      tnum,nbdy,znum,ncom = data.shape

      nbdndes=len(frcbdnodes)

      nc = netCDF4.Dataset(filename,'w',format='NETCDF3_CLASSIC')
      nc.createDimension('time',None)
      nc.createDimension('node',nbdndes)
      nc.createDimension('nLevels',znum)
      nc.createDimension('ntracers',ncom)

      v = nc.createVariable('time','f8',('time',))
      v.long_name = 'simulation time in seconds'
      v[0:tnum] = time
      v.axis = 'T'

      v = nc.createVariable('lon','f8',('node',))
      v.long_name = 'geographic longitude'
      v.units = 'degree_E'
      v.axis = 'X'
      v[:] =  [ setup.lon[ii-1] for ii in frcbdnodes ]

      v = nc.createVariable('lat','f8',('node',))
      v.long_name = 'geographic latitude'
      v.units = 'degree_N'
      v.axis = 'Y'
      v[:] =  [ setup.lat[ii-1] for ii in frcbdnodes ]

      v = nc.createVariable('map_to_global_node','i4',('node',))
      v.long_name = 'global node index'
      v.units = ''
      v[:] = frcbdnodes

      v = nc.createVariable('level','i4',('nLevels',))
      v.long_name = 'depth level'
      v.units = ''
      v.axis = 'Z'
      v.positive = 'up'
      v[:] =  list(range(znum))

      v = nc.createVariable('tracer_concentration','f4',('time','node','nLevels','ntracers'), fill_value=-9999.0)
      v.coordinates = "lon lat"
      v.missing_value= -9999.0

      v[0:tnum,0:nbdy,0:znum,0:ncom] = data

      nc.close()
      return

def create_horizontal_map(lon,lat,glon,glat, mask=None):

    weights=np.zeros((len(lon),4), dtype=float)
    weight_index = np.zeros((len(lon),4), dtype=int)

    for i,l in enumerate(lon):

        dist = np.sqrt((glon - lon[i])**2 + (glat-lat[i])**2)
        minidist = np.argsort(dist)

        if np.all(mask[minidist[:4]]):
            # Choose the one nearest non-missing value neighbour
            weights[i,:] = 0.25 # equal weight
            weight_index[i,:] = minidist[mask[minidist]==False][0]

        else:
            # Choose the four neighbours, if any are missing, this is
            # considered later with ma.average
            invmindist = 1./(dist[minidist[:4]] + 1e-6) # add small eps to avoid overflow
            weights[i,:] = invmindist/sum(invmindist)
            weight_index[i,:] = minidist[:4]

    return weights, weight_index

def create_vertical_map(setup, bdydepth, depth):

    vertical_index=np.zeros((len(bdydepth),len(setup.vgrid)), dtype=int)

    for i,l in enumerate(bdydepth):

        sdepth = np.flip(setup.vgrid*l,axis=0)
        vertical_index[i,:] = np.argmin(np.array([np.abs(sdepth + d) for j, d in enumerate(depth)]),axis=0)

    return vertical_index


def map_variables_vertically(bdy_horizontal, vertical_index):


    out = {}
    for key, value in bdy_horizontal.items():

        nbdy=value.shape[0]

        if value.shape[-1] != 8:
            out[key] = value

        else:

            out[key] =  np.array([value[i,:,vertical_index[i,:]] for i in range(nbdy)])

    return out


def map_variables_horizontally(ncin, weight_index, weights):


    nbdy = weights.shape[0]
    nt = ncin.dimensions.get('time').size
    nz = ncin.dimensions.get('depth').size
    nx = ncin.dimensions.get('x').size
    ny = ncin.dimensions.get('y').size


    maecs = {}
    shape = (nt,nz,nx*ny)


    for key, value in hamoccmaecs.items():


        data = ncin.variables[value][:]
        #ädata[0,0,:,:]=1 # for debugging

        data = data.reshape(shape)[:,:,weight_index]
        maecs[key] = np.array([np.ma.average(np.squeeze(data[:,:,i,:]), weights=weights[i,:], axis=2)
                      for i in range(nbdy)])

        #print(maecs[key][0,0,:])



    return maecs



def move_axes(bdy, nt, nb, nz):
    """ prepares the output to conform to schism.py write_bdy routine that
    expects a certain shape of the output


    (nt,nbdy, znum, ncomponents)

    """

    out = {}

    for key, value in bdy.items():

        s = list(value.shape)
        if not (nt in s and nb in s): continue

        v = np.moveaxis(value,s.index(nt),-s.index(nt)-1)
        out[key] = np.moveaxis(v,s.index(nb),-s.index(nb))


    return out


def read_bctides():

    with open("bctides.in", "r") as f:

        # Search for open boundary specification part
        for line in f:
            if "nope" in line: break

        nbdyseg = int(line.split(' ')[0])
        if nbdyseg != len(setup.bdy_segments):
            print(f"Number of boundary segments is inconsistent {nbdyseg}")

        # Nbdys saves the number of OBC nodes for each variable if their
        # bctides.in parameter is >= 4
        # elev, uv, T, S, then tracer models
        bcsegs=[[],[],[],[],[],[]]
        iseg = -1
        for line in f:
            if not re.search("\d+\s+\d+\s+\d+\s+\d+\s+\d+.*", line): continue
            iseg += 1
            numline = np.array([int(i) for i in line.split("!")[0].split(" ") if len(i)>0], dtype=int)
            for i in range(numline.size-1): # change!
                if numline[i+1]>3: bcsegs[i].extend([iseg])

    return bcsegs


def create_bdy_files(setup,filename):

    ncin = netCDF4.Dataset(filename,"r")

    #(nt,ny,nx,nz) = [d.size for d in ncin.dimensions.values()] # Tim
    (nz,ny,nx,nt) = [d.size for d in ncin.dimensions.values()] # Tim

    glat = ncin.variables["lat"][:]
    glon = ncin.variables["lon"][:]

    flat = glat.reshape(nx*ny)
    flon = glon.reshape(nx*ny)

    # Now determine which of the bdy nodes are needed in forcing, this is
    # indicated by "4" in bctides.in
    # This should be done for all variables, but here is only used for elev

    with open("include.gr3", "r") as f:

        f.readline().rstrip()
        dat = f.readline().split() # setup.n_elemnts
        bdy_nodes = []
        for nn in range(setup.nnodes):
            dat=f.readline().split()
            #include_node.append(float(dat[3])>0)
            if (float(dat[3])>0): bdy_nodes.append(int(dat[0]))
            # ii = 1 basierte indizes der nodes

    lon = [ setup.lon[ii-1] for ii in bdy_nodes ]
    lat = [ setup.lat[ii-1] for ii in bdy_nodes ]
    bdydepth = [ setup.depths[ii-1] for ii in bdy_nodes ]

    mask = ncin.variables["sao"][0,0,:,:].reshape(nx*ny).mask
    weights, weight_index = create_horizontal_map(lon, lat, flon, flat, mask=mask)


    bdy_horizontal = map_variables_horizontally(ncin, weight_index, weights)


    try:
        depth = ncin.variables["depth_2"][:]
    except:
        depth = ncin.variables["depth"][:]

    vertical_index = create_vertical_map(setup, bdydepth, depth)

    bdy = map_variables_vertically(bdy_horizontal, vertical_index)

    bdy["phyN"] = bdy["phyC"]*16/106
    bdy["detN"] = bdy["detC"]*16/106
    bdy["phyP"] = bdy["phyC"]*1/106
    bdy["detP"] = bdy["detC"]*1/106


    # Read and convert time to seconds
    tvar = ncin.variables["time"]
    utime = netcdftime.utime(tvar.units)
    time = utime.num2date(list(tvar[:]))
    stime = netcdftime.utime(tvar.units.replace('hours', 'seconds'))
    time = stime.date2num(time)

    bdy = move_axes(bdy, len(time), len(lon), len(setup.vgrid))

    with open('../fabm/maecs_tracers.yaml') as f:
        tracers = yaml.safe_load(f).get("maecs")

    tracer_concentration=np.zeros((len(time), len(lon), len(setup.vgrid), len(tracers)))-9999.0


    for key,value in tracers.items():
        if not key in bdy: continue

        tracer_concentration[:,:,:,value-1] = bdy[key]

    # Swap depth axis to upward (0 is deepest)
    tracer_concentration = np.flip(tracer_concentration, axis = 2)


    p = pathlib.Path("bdy")
    p.mkdir(parents=True, exist_ok=True)

    write_nudge_netcdf(setup,
                       pathlib.Path(str(p /filename.stem).replace('sns','FBM_maecs_nudge.nc')),
                       time, tracer_concentration, bdy_nodes) #axis=3

    ncin.close()

if __name__ == "__main__":

    if pathlib.Path('hgrid.pickle').exists():
        with open('hgrid.pickle', 'rb') as f:
         setup = pickle.load(f)
    else:
        setup = schism.schism_setup('hgrid.gr3', ll_file = 'hgrid.ll', vgrid_file='vgrid.in')
        with open('hgrid.pickle','wb') as f:
            pickle.dump(setup,f)


    # Manually add vgrid info
    setup.vgrid = np.linspace(-1,0,41)

    if (len(sys.argv) > 1):
        pattern = sys.argv[1]
    else:
        pattern = "../data/166*.nc"
        #pattern = "../data/depth_166*.nc"

    files = pathlib.Path().glob(pattern)

    for i, file in enumerate(files):
        create_bdy_files(setup, file)


#float albo(time, y, x) ;
#	float ano3(time, depth, y, x) ;
#	float denitr(time, y, x) ;
#	float det(time, depth, y, x) ;
#	float graz(time, depth, y, x) ;
#	float intpp(time, y, x) ;
#	float n2fix(time, y, x) ;
#	float o2flux(time, y, x) ;
#	float oxygen(time, depth, y, x) ;
#	float pco2(time, y, x) ;
#	float phosph(time, depth, y, x) ;
#	float phosy(time, depth, y, x) ;
#	float phy(time, depth, y, x) ;
#	float rivrun(time, y, x) ;
#	float sao(time, depth, y, x) ;
#	float silt(time, depth, y, x) ;
#	float tho(time, depth, y, x) ;
#	float uko(time, depth, y, x) ;
#	float vke(time, depth, y, x) ;
#	float zmld(time, y, x) ;
#	float zo(time, y, x) ;
#	float zo2min(time, y, x) ;
#	float zoo(time, depth, y, x) ;