#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 13 12:50:55 2021

@author: lemmen
"""

from schism import schism_setup
import yaml
import numpy as np
import netCDF4 as nc
import os

if __name__ == "__main__":

    try:
        setup = schism_setup('hgrid.gr3', vgrid_file='vgrid.in')
    except:
        print("This program needs 'hgrid.gr3'")

    with open('fabm.yaml') as f:
        fabm = yaml.safe_load(f)

    tracers = dict()

    for key, value in fabm['instances'].items():

        with open(f'../fabm/{key}_tracers.yaml') as f:
            print(f'Load tracer specification for {key}')
            tracers[key] = yaml.safe_load(f)[key]

    n = np.sum([len(value) for key,value in tracers.items()]) + 2
    tr_nd = np.zeros((setup.nnodes,setup.znum,n), dtype=np.float16)

    if os.path.exists('hotstart.nc'):

        ncid = nc.Dataset('hotstart.nc','r')
        tr_nd[:,:,:2] = ncid.variables['tr_nd'][:,:,:2]
        ncid.close()
        print('Added salt and temperature from existing hotstart.nc')
    else: 
        tr_nd[:,:,0] = 15.0
        tr_nd[:,:,1] = 34.0 
        print('Added constant salt=34.0 and temperature=15')

    i = 2
    for key, value in fabm['instances'].items():

        for tracer in tracers[key]:
            if not tracer in value['initialization']:
                pass
            else:
                print(f"Hotstarting {key}/{tracer} with {value['initialization'][tracer]}" )
                tr_nd[:,:,i] = value['initialization'][tracer]
            i = i+1

    setup.write_hotstart(tr_nd,filename=f"hotstart_{'+'.join([key for key in fabm['instances']])}.nc", dtype='f4')
