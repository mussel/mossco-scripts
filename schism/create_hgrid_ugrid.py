#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 13 12:50:55 2021

@author: lemmen
"""

from schism import schism_setup
import numpy as np
import netCDF4
import os

def write_hgrid_ugrid(self,filename,znum=1,ncom=1):
    """
    write hgrid ugrid file
    """

    if self.nelements * self.nnodes < 1:
      print('  setup has no nodes or elements')
      return
      
    nc = netCDF4.Dataset(filename,'w',format='NETCDF4')
    nc.createDimension('nSCHISM_hgrid_node',self.nnodes)
    nc.createDimension('nSCHISM_hgrid_face',self.nelements)
    nc.createDimension('nSCHISM_hgrid_edge',self.nsides)
    nc.createDimension('nMaxSCHISM_hgrid_face_nodes',4)
    nc.createDimension('two',2)
    nc.createDimension('one',1)

    v = nc.createVariable('SCHISM_hgrid_node_x','f4',('nSCHISM_hgrid_node'))
    v.standard_name = 'longitude'
    v.units = 'degrees_east'
    v[:] = self.lon

    v = nc.createVariable('SCHISM_hgrid_node_y','f4',('nSCHISM_hgrid_node'))
    v.standard_name = 'latitude'
    v.units = 'degrees_north'
    v[:] = self.lat

    fnc = np.zeros((self.nelements,4), dtype=np.int) - 99998
    for ie,elem in enumerate(setup.nv): fnc[ie-1,:len(elem)]=elem 
    fnc = fnc - 1

    v = nc.createVariable('SCHISM_hgrid_face_nodes','int',
      ('nSCHISM_hgrid_face','nMaxSCHISM_hgrid_face_nodes'), fill_value=-99999)
    v.cf_role = "face_node_connectivity"
    v.standard_name = "face_node_connectivity"
    v.standard_name = "Face-node connectivity table"
    v.description = "Map every element to the three or four nodes that it surround it counterclockwise" ;
    v.start_index = 0
    v[:] = fnc

    v = nc.createVariable('SCHISM_hgrid_edge_nodes','int',
      ('nSCHISM_hgrid_edge','two'),fill_value=-99999)
    v[:] = np.array(list(setup.side_nodes.values())) - 1
    v.cf_role = "edge_node_connectivity"
    v.description = "Map every edge to the two nodes that it connects"
    v.long_name = "Edge-node connectivity table"
    v.standard_name = "edge_node_connectivity"
    v.start_index = 0

    v = nc.createVariable('SCHISM_hgrid','i4',('one'))
    v.cf_role = "mesh_topology"
    v.topology_dimension = 2
    v.node_coordinates = "SCHISM_hgrid_node_x SCHISM_hgrid_node_y"
    v.face_coordinates = "SCHISM_hgrid_face_x SCHISM_hgrid_face_y"
    v.edge_coordinates = "SCHISM_hgrid_edge_x SCHISM_hgrid_edge_y"
    v.face_node_connectivity="SCHISM_hgrid_face_nodes"
    v.edge_node_connectivity="SCHISM_hgrid_edge_nodes"
    v.long_name = "Topology of 2d unstructured mesh"

    nc.Convention = "CF-1.7, UGRID-1.0"

    nc.close()
    return

if __name__ == "__main__":

    try:
        setup = schism_setup('hgrid.gr3', ll_file='hgrid.ll')
    except:
        print("This program needs 'hgrid.gr3, hgrid.ll'")

    write_hgrid_ugrid(setup, 'hgrid.nc')
