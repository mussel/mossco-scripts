#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 16 19:46:04 2021

@author: Lemmen
"""

import pathlib
import sys
import schism
import pickle
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import cmocean
import pathlib

vardict = {
 'albedo': {'cmap': cmocean.cm.gray, 'scale': 100.0,  'unit': '%'},
 'include': {'cmap': cmocean.cm.gray_r, 'type':'int', },
 'xlsc':  {'cmap': cmocean.cm.gray, 'unit': 'm'},
 'diffmin':  {'cmap': cmocean.cm.matter, 'unit': 'm2 s-1'},
 'drag':  {'cmap': cmocean.cm.matter, 'unit': ''}, # Cd drag coefficient
 'manning':  {'cmap': cmocean.cm.matter, 'unit': '1'}, # Manning's n
 'SAL_nudge': {'cmap': cmocean.cm.gray_r, 'scale': 8640000.0,  'unit': '% d-1', 'vlim': (0,100)},
 'TEM_nudge': {'cmap': cmocean.cm.gray_r, 'scale': 8640000.0,  'unit': '% d-1', 'vlim': (0,100)},
 'watertype': {'cmap': cmocean.cm.phase,  'type': 'int', 'vlim': [1, 7]},
 'windrot_geo2proj': {'cmap': cmocean.cm.phase},
 'diffmax': {'cmap': cmocean.cm.tarn, 'unit': 'm2 s-1'},
 'depths': {'cmap': cmocean.cm.deep, 'unit':'m'},
 'rough': {'cmap': cmocean.cm.deep, 'unit':'m'},
 'manning': {'cmap': cmocean.cm.deep, 'unit':'s m-(1/3)'},

}


class schism_partition(schism.schism_setup):

    def __init__(self, hgrid_file='hgrid.gr3',ll_file='hgrid.ll',vgrid_file='vgrid.in'):
        super.__init__(hgrid_file='hgrid.gr3',ll_file='hgrid.ll',vgrid_file='vgrid.in')

        self.pet = 0


def plot_node_item(setup, item):

    fig, ax = plt.subplots(1,1)

    cmap = vardict.get(item).get('cmap') if item in vardict and 'cmap' in vardict.get(item) else 'viridis'
    data = getattr(setup, 'nnodes')

    (vmin, vmax) = vardict.get(item).get('vlim') if item in vardict and  'vlim' in vardict.get(item) else (np.min(data), np.max(data))
    unit = vardict.get(item).get('unit') if item in vardict and  'unit' in vardict.get(item) else ''

    if hasattr(setup, 'lon') and hasattr(setup, 'lat'):
      plt.tripcolor(setup.lon,setup.lat, setup.triangles, data,
                  cmap=cmap, vmin=vmin, vmax=vmax)
    else:
      plt.tripcolor(setup.x,setup.y, setup.triangles, data,
                  cmap=cmap, vmin=vmin, vmax=vmax)

    cb = plt.colorbar()
    cb.set_label(item + ' (' + unit + ')')
    plot_inset_histogram(ax, data)

    plt.savefig("schism_setup_" + item + ".pdf")
    plt.savefig("schism_setup_" + item + ".png", dpi=300)

def plot_mesh(setup):

    plt.figure()

    try:
      plt.triplot(setup.lon,setup.lat,setup.triangles, lw=0.01)
    except AttributeError:
      plt.triplot(setup.x,setup.y,setup.triangles, lw=0.01)
    plt.savefig("schism_setup_mesh.pdf")

def read_gr3(setup, name):

    filename = pathlib.Path(str(setup.hgrid_file).replace(
        'hgrid.gr3', name + '.gr3'))

    try:
        df = pd.read_csv(filename, header=None, skiprows=2, #index_col=0,
                         nrows=setup.nnodes, delim_whitespace=True, usecols=[3])
        return df[3]

    except:
        return None

def main():
    setup_dir = '.'
    #setup_dir = "/Users/Lemmen/setups/nsh/nsh-benjamin"
    #setup_dir = "/Users/Lemmen/devel/schism/schism_verification_tests/Test_CORIE"
    #setup_dir = "/Users/Lemmen/devel/schism/schism_verification_tests/Test_HeatConsv_TVD"

    if len(sys.argv)>1:
        setup_dir = sys.argv[0]

    if pathlib.Path('hgrid.pickle').exists():
        with open('hgrid.pickle', 'rb') as f:
            setup = pickle.load(f)
    else:
        setup = schism.schism_setup(pathlib.Path(setup_dir) / 'hgrid.gr3',
            ll_file = pathlib.Path(setup_dir) / 'hgrid.ll',
            vgrid_file=pathlib.Path(setup_dir) / 'vgrid.in'
        )

    dir(setup)
    plot_node_item(setup, 'depth')

    return setup

if __name__ == "__main__":
    setup = main()
