import cartopy.crs as ccrs
import matplotlib.pyplot as plt

def clp_basemap(latlim=None, lonlim=None, ax=None):

    fig = plt.gcf()
    if ax is None:
        ax = plt.gca()

    ax.remove()

    if (latlim is None) or (lonlim is None):
        ax = plt.axes(projection=ccrs.PlateCarree())
        ax.set_global()

    else:
        latlim=tuple(latlim)
        lonlim=tuple(lonlim)

        ax = plt.axes(projection=ccrs.PlateCarree())
        ax.set_extent(lonlim + latlim)

    ax.coastlines()

    return fig, ax


if __name__ == '__main__':

    lonlim = (5,10)
    latlim = (52,55)

    fig,ax = clp_basemap(lonlim=lonlim, latlim=latlim)

    plt.show()