#!/usr/bin/env python

"""
SCHISM setup class
"""

__author__  = "Richard Hofmeister"
__license__ = "GNU GPL v2.0"

import numpy as np

class schism_setup(object):

  def __init__(self,hgrid_file='hgrid.gr3',ll_file='hgrid.ll',vgrid_file='vgrid.in'):
      self.hgrid_file=hgrid_file

      # parse hgrid file
      f = open(hgrid_file)
      self.description = f.readline().rstrip()
      dat = f.readline().split()
      self.nelements = int(dat[0])
      self.nnodes = int(dat[1])

      n=[]
      x=[]
      y=[]
      d=[]
      self.depthsdict={}
      self.xdict={}
      self.ydict={}
      for nn in range(self.nnodes):
        dat=f.readline().split()
        n.append(int(dat[0]))
        x.append(float(dat[1]))
        y.append(float(dat[2]))
        d.append(float(dat[3]))
        self.depthsdict[n[-1]] = d[-1]
        self.ydict[n[-1]] = y[-1]
        self.xdict[n[-1]] = x[-1]
      self.inodes = n
      self.x = x
      self.y = y
      self.depths = d

      n=[]
      nv = []
      nvdict = {}
      for nn in range(self.nelements):
        dat=f.readline().split()
        n.append(int(dat[0]))
        nvnum = int(dat[1])
        nv.append([ int(ii) for ii in dat[2:2+nvnum]])
        nvdict[n[-1]] = nv[-1]
      self.ielement = n
      self.nv = nv
      self.nvdict = nvdict


      # get resolution of elements
      res = {}
      dmin = {}
      import numpy as np
      for el in self.nvdict:
        inodes = self.nvdict[el]
        x = [self.xdict[ii] for ii in inodes]
        y = [self.ydict[ii] for ii in inodes]
        res[el] = (np.sqrt((x[1]-x[0])**2 + (y[1]-y[0])**2) + np.sqrt((x[2]-x[1])**2 + (y[2]-y[1])**2) + np.sqrt((x[0]-x[2])**2 + (y[0]-y[2])**2))/3.0
        dmin[el] = min(np.array( [np.sqrt((x[1]-x[0])**2 + (y[1]-y[0])**2), np.sqrt((x[2]-x[1])**2 + (y[2]-y[1])**2) , np.sqrt((x[0]-x[2])**2 + (y[0]-y[2])**2)]))
      self.resolution_by_element = dict(res)
      self.min_sidelength_by_element = dict(res)

      # compute sides
      # first get sequence array
      self.nx = {}
      self.nx[3] = [[1,2],[2,0],[0,1]]
      self.nx[4] = [[1,2],[2,3],[3,0],[0,1]]

      # get inverse nv - neighbouring elements
      self.node_neighbour_elements = { i:[] for i in self.inodes }
      self.n_node_neighbours = { i:0 for i in self.inodes }
      for i,nv in zip(self.ielement,self.nv):
        for nnode in nv:
          self.n_node_neighbours[nnode] += 1
          self.node_neighbour_elements[nnode].append(i)
      # find neighbouring elements around element (ic3)
      self.element_sides={}
      self.side_nodes={}
      for i,nv in zip(self.ielement,self.nv):
        isides = []
        # loop around element for existing sides
        for iloop,(ind1,ind2) in enumerate(self.nx[len(nv)]):
          iside = 0
          nd1,nd2 = nv[ind1],nv[ind2]
          for checkelement in self.node_neighbour_elements[nd1]:
            if (checkelement != i) and (nd2 in self.nvdict[checkelement]):
              iside = checkelement
              break
          isides.append(iside)
        self.element_sides[i] = isides
      # count sides
      self.nsides = 0
      element_ids = sorted([int(k) for k in self.element_sides.keys()])
#      element_ids.sort()
      for i in element_ids:
        for ii,iside in enumerate(self.element_sides[i]):
          if iside==0 or i<iside:
            self.nsides += 1
            iinds = self.nx[len(self.element_sides[i])][ii]
            self.side_nodes[self.nsides] = [self.nvdict[i][iinds[0]],self.nvdict[i][iinds[1]]]

      # average resolution_by_element on nodes
      self.resolution_by_nodes = {}
      for inode in self.node_neighbour_elements:
        elids = self.node_neighbour_elements[inode]
        self.resolution_by_nodes[inode] = np.asarray([self.resolution_by_element[ii] for ii in self.node_neighbour_elements[inode]]).mean()

      self.num_bdy_segments = int(f.readline().split()[0])
      self.num_bdy_nodes = int(f.readline().split()[0])
      self.bdy_segments=[]
      self.bdy_nodes=[]
      for iseg in range(self.num_bdy_segments):
        nlines = int(f.readline().split()[0])
        self.bdy_segments.append( [int(f.readline()) for nn in range(nlines)] )
        self.bdy_nodes.extend(self.bdy_segments[-1])

      self.num_land_segments = int(f.readline().split()[0])
      self.num_land_nodes = int(f.readline().split()[0])
      self.land_nodes=[]
      self.island_segments=[]
      self.land_segments=[]
      for iseg in range(self.num_land_segments):
        dat = f.readline().split()
        nlines = int(dat[0])
        if int(dat[1])==0:
          self.land_segments.append( [int(f.readline()) for nn in range(nlines)] )
          self.land_nodes.extend(self.land_segments[-1])
        elif int(dat[1])==1:
          self.island_segments.append( [int(f.readline()) for nn in range(nlines)] )
          self.land_nodes.extend(self.island_segments[-1])

      f.close()

      # parse hgrid.ll file
      try:
        f = open(ll_file)
        line = f.readline().rstrip()
        dat = f.readline().split()
        ll_nelements = int(dat[0])
        ll_nnodes = int(dat[1])

        nll = []
        lon=[]
        lat=[]
        self.londict={}
        self.latdict={}
        for nn in range(self.nnodes):
          dat=f.readline().split()
          nll.append(int(dat[0]))
          lon.append(float(dat[1]))
          lat.append(float(dat[2]))
          self.londict[nll[-1]]=lon[-1]
          self.latdict[nll[-1]]=lat[-1]
        self.ill = nll
        self.lon = lon
        self.lat = lat
        f.close()

        # compute minimum side length in
        # element wise local projection (for cfl computation)
        res = {}
        dmin = {}
        import numpy as np

        for el in self.ielement:
            inodes = self.nv[el-1]
            x,y=self.cpp_proj_elemNodes(inodes)
            # convert lon lat to local projection for element
            res[el] = (np.sqrt((x[1]-x[0])**2 + (y[1]-y[0])**2) + np.sqrt((x[2]-x[1])**2 + (y[2]-y[1])**2) + np.sqrt((x[0]-x[2])**2 + (y[0]-y[2])**2))/3.0
            dmin[el] = min(np.array( [np.sqrt((x[1]-x[0])**2 + (y[1]-y[0])**2), np.sqrt((x[2]-x[1])**2 + (y[2]-y[1])**2) , np.sqrt((x[0]-x[2])**2 + (y[0]-y[2])**2)]))
        self.cpp_resolution_by_element = res
        self.min_cpp_sidelength_by_element = dmin

      except:
        print('  no hgrid.ll available')

      try:
        self.parse_vgrid(vgrid_file=vgrid_file)
      except:
        print('  no vgrid.in available')

      self.node_tree_xy = None
      self.node_tree_latlon = None
      self.element_tree_xy = None
      self.element_tree_latlon = None
      self.element_depth = None

  def parse_vgrid(self, vgrid_file='vgrid.in'):
    import numpy as np

    with open(vgrid_file, 'r') as f:        
        vtype = int(f.readline().split(' ')[0])
        if vtype != 2:
            print('vgrid.in type not implemented')
            return
        
        line = f.readline().split(' ')
        nvrt, kz, h_s = (int(line[0]), int(line[1]), float(line[2]))
        self.znum = nvrt
        
        # Skip z layers for now
        for i in range(kz + 3): f.readline()
        
        a = {}
        self.bidx = {}
        for line in f.readlines():
            sigma1d = -9999.*np.ones((nvrt,))
            data = line.split()
            self.bidx[int(data[0])] = int(data[1])
            sigma1d[int(data[1])-1:] = np.asarray([float(ii) for ii in data[2:]])
            a[int(data[0])] = np.ma.masked_equal(sigma1d,-9999.)
        self.vgrid = a

  def dump_hgridll(self,filename='hgrid_new.ll'):
    f = open(filename,'w')
    f.write('%s\n'%filename)
    f.write('%d %d\n'%(self.nelements,self.nnodes))
    # write nodes
    for n,x,y,d in zip(self.inodes,self.lon,self.lat,self.depths):
      f.write('%d %0.6f %0.6f %0.2f\n'%(n,x,y,d))

    # write elements
    for n,nv in zip(self.ielement,self.nv):
      f.write('%d %d '%(n,len(nv)))
      for nvi in nv:
        f.write('%d '%nvi)
      f.write('\n')
    # write boundaries
    f.close()


  def dump_hgridgr3(self,filename='hgrid_new.gr3',comment='grid_transfer'):
      f = open(filename,'w')
      f.write('%s - %s\n'%(filename,comment))
      f.write('%d %d\n'%(self.nelements,self.nnodes))
      # write nodes
      for n,x,y,d in zip(self.inodes,self.x,self.y,self.depths):
          f.write('%d %0.6f %0.6f %0.2f\n'%(n,x,y,d))

      # write elements
      for n,nv in zip(self.ielement,self.nv):
          f.write('%d %d '%(n,len(nv)))
          for nvi in nv:
              f.write('%d '%nvi)
          f.write('\n')

      # write boundaries
      f.write('%i = Number of open boundaries\n'%self.num_bdy_segments)
      f.write('%i = Total number of open boundary nodes\n'%self.num_bdy_nodes)

      for iseg,seg in enumerate(self.bdy_segments):
          f.write('%i 0 = Number of nodes for open boundary %i\n'%(len(seg),iseg+1))
          for inode in range(len(seg)):
              f.write('%i\n'%(seg[inode]))

      f.write('%i = number of land boundaries\n'%self.num_land_segments)
      f.write('%i = Total number of land boundary nodes\n'%self.num_land_nodes)

      for iseg,seg in enumerate(self.land_segments):
          f.write('%i 0 = Number of nodes for land boundary %i\n'%(len(seg),iseg+1))
          for inode in range(len(seg)):
              f.write('%i\n'%(seg[inode]))

      for iseg,seg in enumerate(self.island_segments):
          f.write('%i 1 = Number of nodes for island boundary %i\n'%(len(seg),iseg+1))
          for inode in range(len(seg)):
              f.write('%i\n'%(seg[inode]))
      f.close()




  def get_bdy_latlon(self):
      bdylon = [ self.lon[ii-1] for ii in self.bdy_nodes ]
      bdylat = [ self.lat[ii-1] for ii in self.bdy_nodes ]
      return (bdylon,bdylat)

  def plot_domain_boundaries(self):
      from pylab import figure,plot,show,legend,xlabel,ylabel
      f = figure()

      label='open boundary'
      for seg in self.bdy_segments:
        lon = [ self.lon[ii-1] for ii in seg ]
        lat = [ self.lat[ii-1] for ii in seg ]
        plot( lon,lat, 'ro-', ms=0.5, label=label, markeredgecolor='r')
        label=''

      label='land boundary'
      for seg in self.land_segments:
        lon = [ self.lon[ii-1] for ii in seg ]
        lat = [ self.lat[ii-1] for ii in seg ]
        lcol = (0.6,0.6,0.6)
        plot( lon,lat, 'o-',color=lcol, ms=0.1, label=label, markeredgecolor=lcol)
        label=''

      label='island boundary'
      for seg in self.island_segments:
        lon = [ self.lon[ii-1] for ii in seg ]
        lat = [ self.lat[ii-1] for ii in seg ]
        lcol = (0.3,0.3,0.3)
        plot( lon,lat, 'o-',color=lcol, ms=0.1, label=label, markeredgecolor=lcol)
        label=''

      legend(loc='lower right',frameon=False)
      xlabel('longitude [degE]')
      ylabel('latitude [degN]')
      show()

  def signed_area(self,nodelist):
      x1,y1 = (self.xdict[nodelist[0]],self.ydict[nodelist[0]])
      x2,y2 = (self.xdict[nodelist[1]],self.ydict[nodelist[1]])
      x3,y3 = (self.xdict[nodelist[2]],self.ydict[nodelist[2]])
      return ((x1-x3)*(y2-y3)-(x2-x3)*(y1-y3))/2.0


  def proj_area(self,nodelist):
      import numpy as np
      x1,y1 = (self.londict[nodelist[0]],self.latdict[nodelist[0]])
      x2,y2 = (self.londict[nodelist[1]],self.latdict[nodelist[1]])
      x3,y3 = (self.londict[nodelist[2]],self.latdict[nodelist[2]])
      #
      rad=np.pi/180
      rlambda=(x1+x2+x3)/3.0*rad # x,y correct order
      phi=(y1+y2+y3)/3.0*rad
      #
      r=6378206.4
      x1=r*(x1*rad-rlambda)*np.cos(phi)
      x2=r*(x2*rad-rlambda)*np.cos(phi)
      x3=r*(x3*rad-rlambda)*np.cos(phi)
      #
      y1=r*y1*rad
      y2=r*y2*rad
      y3=r*y3*rad
      return ((x1-x3)*(y2-y3)-(x2-x3)*(y1-y3))/2.0


  def dump_gr3(self,filename,const=0.0,comment='gr3 by create_ic.py'):
      f = open(filename,'w')
      f.write('%s\n'%comment)
      f.write('%d %d\n'%(self.nelements,self.nnodes))
      for i,x,y in zip(self.inodes,self.x,self.y):
        f.write('%d %0.2f %0.2f %0.5f\n'%(i,x,y,const))
      f.close()

  def dump_gr3_spat_var(self,filename,nodevalues,comment='gr3 by create_ic.py'):
      f = open(filename,'w')
      f.write('%s\n'%comment)
      f.write('%d %d\n'%(self.nelements,self.nnodes))
      for i,x,y,value in zip(self.inodes,self.x,self.y,nodevalues):
        f.write('%d %0.2f %0.2f %0.5f\n'%(i,x,y,value))
      f.close()



  def dump_tvd_prop(self):
      f = open('tvd.prop','w')
      for i in self.ielement:
        f.write('%d 1\n'%i)
      f.close()


  def init_node_tree(self,latlon=True):
    #print('  build node tree')
    from scipy.spatial import cKDTree
    if latlon:
      self.node_tree_latlon = cKDTree(zip(self.lon,self.lat))
    else:
      self.node_tree_xy = cKDTree(zip(self.x,self.y))

  def init_element_tree(self,latlon=True):
    """
    build element tree for xy or latlon coordinates
    (default: latlon=True)
    """
    from scipy.spatial import cKDTree

    #print('  schism.py: build element tree')
    if self.element_depth == None:
      self.element_depth={}
      calc_depths = True
    else:
      calc_depths = False

    if latlon:
      self.element_lon={}
      self.element_lat={}
      for el in self.nvdict:
        self.element_lon[el] = sum([self.londict[idx] for idx in self.nvdict[el]])/len(self.nvdict[el])
        self.element_lat[el] = sum([self.latdict[idx] for idx in self.nvdict[el]])/len(self.nvdict[el])
        if calc_depths:
          self.element_depth[el] = sum([self.depthsdict[idx] for idx in self.nvdict[el]])/len(self.nvdict[el])
      self.element_tree_latlon = cKDTree(zip(self.element_lon.values(),self.element_lat.values()))
      self.element_tree_ids = self.element_lon.keys()
    else:
      self.element_x={}
      self.element_y={}
      for el in self.nvdict:
        self.element_x[el] = sum([self.xdict[idx] for idx in self.nvdict[el]])/len(self.nvdict[el])
        self.element_y[el] = sum([self.ydict[idx] for idx in self.nvdict[el]])/len(self.nvdict[el])
        if calc_depths:
          self.element_depth[el] = sum([self.depthsdict[idx] for idx in self.nvdict[el]])/len(self.nvdict[el])
      self.element_tree_xy = cKDTree(zip(self.element_x.values(),self.element_y.values()))
      self.element_tree_ids = self.element_x.keys()


  def find_nearest_node(self,x,y,latlon=True):
    """
    find nearest node for given coordinate,
    returns the node id
    """
    ridx=-1
    if latlon:
      if self.node_tree_latlon==None:
        self.init_node_tree(latlon=True)
      d,idx = self.node_tree_latlon.query((x,y),k=1)
      ridx = self.ill[idx]
    else:
      if self.node_tree_latlon==None:
        self.init_node_tree(latlon=False)
      d,idx = self.node_tree_latlon.query((x,y),k=1)
      ridx = self.inodes[idx]
    return ridx

  def find_nearest_element(self,x,y,latlon=True,mindepth=3.0,numcells=5):
    """
    give coordinates and find nearest element,
    returns the element id
    """
    ridx=-1
    numcells=numcells
    while ridx<0:
      if latlon:
        if self.element_tree_latlon==None:
          self.init_element_tree(latlon=True)
        d,idx = self.element_tree_latlon.query((x,y),k=numcells)
      else:
        if self.element_tree_xy==None:
          self.init_element_tree(latlon=False)
        d,idx = self.element_tree_xy.query((x,y),k=numcells)
      depths = np.array([self.element_depth[self.element_tree_ids[searchid]] for searchid in idx])
      maxidx = np.argmax(depths)
      if depths[maxidx]>mindepth:
        ridx = self.element_tree_ids[idx[maxidx]]
        #print('  info: found cell with depth>%0.1fm in %d nearest cells'%(mindepth,numcells))
      else:
        # continue iterating:
        numcells = numcells*2
        if numcells > 10000:
          print('  warning: cannot find cell with depth>%0.1fm'%(mindepth))
          break
    return ridx



  def cpp_proj_elemNodes(self,nodelist):
      # create cntral point porjection for element of lon lat
      import numpy as np
      x1,y1 = (self.londict[nodelist[0]],self.latdict[nodelist[0]])
      x2,y2 = (self.londict[nodelist[1]],self.latdict[nodelist[1]])
      x3,y3 = (self.londict[nodelist[2]],self.latdict[nodelist[2]])
      #
      rad=np.pi/180
      rlambda=(x1+x2+x3)/3.0*rad # x,y correct order
      phi=(y1+y2+y3)/3.0*rad
      #
      r=6378206.4
      x1=r*(x1*rad-rlambda)*np.cos(phi)
      x2=r*(x2*rad-rlambda)*np.cos(phi)
      x3=r*(x3*rad-rlambda)*np.cos(phi)
      #
      y1=r*y1*rad
      y2=r*y2*rad
      y3=r*y3*rad

      return [x1,x2,x3],[y1,y2,y3]



  def compute_cfl(self,dt,grid='gr3'):


    import numpy as np

    if grid=='cpp':
        dxs=np.asarray(self.min_cpp_sidelength_by_element.values())
    elif grid=='gr3':
        dxs=np.asarray(self.min_sidelength_by_element.values())
    else:
        print('specify grid as either gr3 or cpp')
    return

    g=9.81
    cfl_by_elements=np.zeros(self.nelements)

    # calc depth at nodes
    maxDepth=np.zeros(self.nelements)

    for ielement in self.ielement:
        maxDepth[ielement-1]=max([self.depths[idx-1] for idx in self.nv[ielement-1]])



    #for ielem in self.ielement:
    cfl_by_elements=np.sqrt(g*maxDepth)*dt/dxs

    return cfl_by_elements

  def write_hotstart(self,tr_nd,filename='hotstart.nc',dtype='f8', time=0.0,iths=0,ifile=0,elev=0.0,uvel=0.0,vvel=0.0):
    """
    write hotstart.nc from given tracer concentrations on nodes
    tracer concentrations on sides and elements will be interpolated
    tr_nd.shape has to be (nnodes,nvrt,ntracers)
    """
    inum,znum,ntracers = tr_nd.shape
    if ((inum,znum) != (self.nnodes,self.znum)):
      print('  shape(tr_nd) = (%d,%d) while setup requires (%d,%d)'%(inum,znum,self.nnodes,self.znum))

    import netCDF4
    nc = netCDF4.Dataset(filename,'w',format='NETCDF4_CLASSIC')
    nc.createDimension('node',self.nnodes)
    nc.createDimension('elem',self.nelements)
    nc.createDimension('side',self.nsides)
    nc.createDimension('nVert',self.znum)
    nc.createDimension('ntracers',ntracers)
    nc.createDimension('one',1)
    nc.createDimension('three',3)

    v = nc.createVariable('time','f8',('one',))
    v[:] = time
    v = nc.createVariable('iths','i',('one',))
    v[:] = iths
    v = nc.createVariable('ifile','i',('one',))
    v[:] = ifile
    v = nc.createVariable('idry_e','i',('elem',))
    v[:] = 0
    v = nc.createVariable('idry_s','i',('side',))
    v[:] = 0
    v = nc.createVariable('idry','i',('node',))
    v[:] = 0
    v = nc.createVariable('eta2',dtype,('node',))
    v[:] = elev
    v = nc.createVariable('we',dtype,('elem','nVert'))
    v[:] = 0.0
    v = nc.createVariable('su2',dtype,('side','nVert'))
    v[:] = uvel
    v = nc.createVariable('sv2',dtype,('side','nVert'))
    v[:] = vvel
    v = nc.createVariable('q2',dtype,('node','nVert'))
    v[:] = 0.0
    v = nc.createVariable('xl',dtype,('node','nVert'))
    v[:] = 0.0
    v = nc.createVariable('dfv',dtype,('node','nVert'))
    v[:] = 0.0
    v = nc.createVariable('dfh',dtype,('node','nVert'))
    v[:] = 0.0
    v = nc.createVariable('dfq1',dtype,('node','nVert'))
    v[:] = 0.0
    v = nc.createVariable('dfq2',dtype,('node','nVert'))
    v[:] = 0.0
    nc.sync()

    # write tracer concentrations on nodes
    v = nc.createVariable('tr_nd',dtype,('node','nVert','ntracers'))
    v[:] = tr_nd
    nc.sync()
    v = nc.createVariable('tr_nd0',dtype,('node','nVert','ntracers'))
    v[:] = tr_nd
    nc.sync()

    # write tracer concentrations on elemenelementsnts
    v = nc.createVariable('tr_el',dtype,('elem','nVert','ntracers'))
    for ie in self.nvdict:
      inds = self.nvdict[ie]
      tr_coll = [tr_nd[ind-1] for ind in inds]
      tr_el = np.mean(tr_coll,axis=0)
      v[ie-1] = tr_el
    nc.sync()
    nc.close()


  def write_bdy_netcdf(self, filename, time, data, frcbdnodes=[]):
      """
      write boundary data for schism setup
      """
      import netCDF4

      if self.num_bdy_nodes==0:
        print('  setup has no open boundaries')
        return

      datadims = len(data.shape)
      if datadims==2:
        tnum,nbdy = data.shape
        znum = 1
        ncom = 1
      elif datadims==3:
        tnum,nbdy,ncom = data.shape
        znum = 1
      elif datadims==4:
        tnum,nbdy,znum,ncom = data.shape

      if len(frcbdnodes)>0:
        nbdndes=len(frcbdnodes)
      else:
        nbdndes=self.num_bdy_nodes
        frcbdnodes = self.bdy_nodes

      nc = netCDF4.Dataset(filename,'w',format='NETCDF3_CLASSIC')
      nc.createDimension('time',None)
      nc.createDimension('nOpenBndNodes',nbdndes)
      nc.createDimension('nLevels',znum)
      nc.createDimension('nComponents',ncom)
      nc.createDimension('one',1)

      v = nc.createVariable('time_step','f4',('one',))
      v.long_name = 'time step in seconds'
      v[:] = time[1]-time[0]

      v = nc.createVariable('time','f8',('time',))
      v.long_name = 'simulation time in seconds'
      v[0:tnum] = time
      v.axis = 'T'

    
      v = nc.createVariable('lon','f8',('nOpenBndNodes',))
      v.long_name = 'geographic longitude'
      v.units = 'degree_E'
      v.axis = 'X'
      v[:] =  [ self.lon[ii-1] for ii in frcbdnodes ]
            
      v = nc.createVariable('lat','f8',('nOpenBndNodes',))
      v.long_name = 'geographic latitude'
      v.units = 'degree_N'
      v.axis = 'Y'
      v[:] =  [ self.lat[ii-1] for ii in frcbdnodes ]

      v = nc.createVariable('level','i4',('nLevels',))
      v.long_name = 'depth level'
      v.units = ''
      v.axis = 'Z'
      v.positive = 'downwards'
      v[:] =  list(range(znum))

      v = nc.createVariable('time_series','f4',('time','nOpenBndNodes','nLevels','nComponents'))
      v.coordinates = "lon lat"
      if datadims==2:
        v[0:tnum,0:nbdy,0,0] = data
      elif datadims==3:
        v[0:tnum,0:nbdy,0,0:ncom] = data
      elif datadims==4:
        #v[0:tnum] = data
        v[0:tnum,0:nbdy,0:znum,0:ncom] = data

      nc.close()
      return


class schism_output():
    import netCDF4
    nc = None

    def __init__(self,filename):
      """
      read output filename and initialize grid
      """
      import netCDF4
      from netcdftime import utime
      self.nc = netCDF4.Dataset(filename)
      self.ncv = self.nc.variables
      self.lon = self.ncv['SCHISM_hgrid_node_x'][:]
      self.lat = self.ncv['SCHISM_hgrid_node_y'][:]
      self.nodeids = np.arange(len(self.lon))
      self.nv = self.ncv['SCHISM_hgrid_face_nodes'][:,:3]-1
      self.time = self.ncv['time'][:] # s
      self.ut = utime(self.ncv['time'].units)
      self.dates = self.ut.num2date(self.time)
      self.node_tree_latlon = None

    def init_node_tree(self,latlon=True):
      """
      build a node tree using cKDTree
      for a quick search for node coordinates
      """
      from scipy.spatial import cKDTree
      if latlon:
        self.node_tree_latlon = cKDTree(zip(self.lon,self.lat))
      else:
        self.node_tree_xy = cKDTree(zip(self.x,self.y))

    def find_nearest_node(self,x,y,latlon=True):
      """
      find nearest node for given coordinate,
      returns the node id
      """
      ridx=-1
      if latlon:
        if self.node_tree_latlon==None:
          self.init_node_tree(latlon=True)
        d,idx = self.node_tree_latlon.query((x,y),k=1)
        ridx = self.nodeids[idx]
      else:
        if self.node_tree_latlon==None:
           self.init_node_tree(latlon=False)
        d,idx = self.node_tree_latlon.query((x,y),k=1)
        ridx = self.inodes[idx]
      return ridx



if __name__ == '__main__':

    from pylab import *
    setup = schism_setup('hgrid.gr3',ll_file='hgrid.gr3', vgrid_file='vgrid.in')
    # plot domain
    setup.plot_domain_boundaries()

    #triplot(setup.x,setup.y,asarray(setup.nv)-1)

    show()

    if False:
      # read elevation boundaries
      t,e = setup.bdy_array('elev2D.th')
      figure()
      plot(t[:],e[:,50])
      show()
