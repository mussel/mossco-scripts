import pandas as pd

wNC = 14.0/12.0  # weight of N to C
wPC = 31.0/16.0

mwC = 1.0/12.0

mNC = 16.0/106.0 # Redfield N to C
mPC = 1.0/106.0

if __name__ == "__main__":

  ecosmo_no3 = pd.read_csv("FBM_1.th", sep=" ", index_col=0, header=None)
  ecosmo_nh4 = pd.read_csv("FBM_2.th", sep=" ", index_col=0, header=None)
  ecosmo_pho = pd.read_csv("FBM_3.th", sep=" ", index_col=0, header=None)
  ecosmo_sil = pd.read_csv("FBM_4.th", sep=" ", index_col=0, header=None)
  ecosmo_oxy = pd.read_csv("FBM_5.th", sep=" ", index_col=0, header=None)
  ecosmo_fla = pd.read_csv("FBM_6.th", sep=" ", index_col=0, header=None)
  ecosmo_dia = pd.read_csv("FBM_7.th", sep=" ", index_col=0, header=None)
  ecosmo_bg = pd.read_csv("FBM_8.th", sep=" ", index_col=0, header=None)
  ecosmo_bgchl = pd.read_csv("FBM_9.th", sep=" ", index_col=0, header=None)
  ecosmo_diachl = pd.read_csv("FBM_10.th", sep=" ", index_col=0, header=None)
  ecosmo_flachl = pd.read_csv("FBM_11.th", sep=" ", index_col=0, header=None)
  ecosmo_microzoo = pd.read_csv("FBM_12.th", sep=" ", index_col=0, header=None)
  ecosmo_mesozoo = pd.read_csv("FBM_13.th", sep=" ", index_col=0, header=None)
  ecosmo_det = pd.read_csv("FBM_14.th", sep=" ", index_col=0, header=None)
  ecosmo_opa = pd.read_csv("FBM_15.th", sep=" ", index_col=0, header=None)
  ecosmo_dom = pd.read_csv("FBM_16.th", sep=" ", index_col=0, header=None)

  maecs_nutN = (ecosmo_no3 + ecosmo_nh4) * wNC * mNC
  maecs_phyC = (ecosmo_fla + ecosmo_dia + ecosmo_bg) * mwC
  maecs_phyN = maecs_phyC * mNC
  maecs_detC = ecosmo_det * mwC
  maecs_detN = maecs_detC * mNC
  maecs_domC = ecosmo_dom * mwC
  maecs_domN = maecs_domC * mNC
  maecs_Rub  = maecs_domC * 0.0
  maecs_Chl  = (ecosmo_diachl + ecosmo_flachl + ecosmo_bgchl)
  maecs_nutP = ecosmo_pho * wPC * mPC
  maecs_phyP = maecs_phyC * mPC
  maecs_detP = maecs_detC * mPC
  maecs_domP = maecs_domC * mPC
  maecs_zooC = (ecosmo_microzoo + ecosmo_mesozoo ) * mwC
  maecs_nh3  = ecosmo_nh4 * (1/18.0) * wNC
  maecs_oxy  = ecosmo_oxy
  maecs_odu  = maecs_oxy * 0.0
  maecs_vir  = maecs_odu

  maecs_nutN.to_csv('FBM_maecs_nutN.th', sep=" ", header=None)
  maecs_phyC.to_csv('FBM_maecs_phyC.th', sep=" ", header=None)
  maecs_phyN.to_csv('FBM_maecs_phyN.th', sep=" ", header=None)
  maecs_detC.to_csv('FBM_maecs_detC.th', sep=" ", header=None)
  maecs_detN.to_csv('FBM_maecs_detN.th', sep=" ", header=None)
  maecs_domC.to_csv('FBM_maecs_domC.th', sep=" ", header=None)
  maecs_domN.to_csv('FBM_maecs_domN.th', sep=" ", header=None)
  maecs_Chl.to_csv('FBM_maecs_Chl.th', sep=" ", header=None)
  maecs_Rub.to_csv('FBM_maecs_Rub.th', sep=" ", header=None)
  maecs_nutP.to_csv('FBM_maecs_nutP.th', sep=" ", header=None)
  maecs_phyP.to_csv('FBM_maecs_phyP.th', sep=" ", header=None)
  maecs_detP.to_csv('FBM_maecs_detP.th', sep=" ", header=None)
  maecs_domP.to_csv('FBM_maecs_domP.th', sep=" ", header=None)
  maecs_zooC.to_csv('FBM_maecs_zooC.th', sep=" ", header=None)
  maecs_nh3.to_csv('FBM_maecs_nh3.th', sep=" ", header=None)
  maecs_oxy.to_csv('FBM_maecs_oxy.th', sep=" ", header=None)
  maecs_odu.to_csv('FBM_maecs_odu.th', sep=" ", header=None)
  maecs_vir.to_csv('FBM_maecs_vir.th', sep=" ", header=None)

