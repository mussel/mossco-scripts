#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 28 18:11:57 2021

@author: Lemmen
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd


# !       Read in water type; the values for R, d_1, d_2 are given below
# !       solar flux= R*exp(z/d_1))+(1-R)*exp(z/d_2) (d_[1,2] are attentuation depths; smaller values for muddier water)
# !       1: 0.58 0.35 23 (Jerlov type I)
# !       2: 0.62 0.60 20 (Jerlov type IA)
# !       3: 0.67 1.00 17 (Jerlov type IB)
# !       4: 0.77 1.50 14 (Jerlov type II)
# !       5: 0.78 1.40 7.9 (Jerlov type III)
# !       6: 0.62 1.50 20 (Paulson and Simpson 1977; similar to type IA)
# !       7: 0.80 0.90 2.1 (Mike Z.'s choice for estuary)

watertypes = {1: [0.58, 0.35, 23, "Jerlov type I"],
              2: [0.62, 0.60, 20, " Jerlov type IA"],
              3: [0.67, 1.00, 17, "Jerlov type IB"],
              4: [0.77, 1.50, 14, "Jerlov type II"],
              5: [0.78, 1.40, 7.9, "Jerlov type III"],
              6: [0.62, 1.50, 20,  "Paulson & Simpson 1977"],
              7: [0.80, 0.90, 2.1, "Mike Z.'s estuary"]
              }



def profile(depth, watertype):

    w = watertypes[watertype]
    return w[0]* np.exp(depth / w[1]) + (1-w[0]) * np.exp(depth / w[2])



if __name__ == "__main__":

    fig, axs = plt.subplots(nrows=1, ncols=2, sharey=True)

    z = np.linspace(-15,0,50)

    for k, v in watertypes.items():
        j = 0 if k < 5 else 1
        axs[j].plot(profile(z,k), z, label=f"{k}: {v[3]}")

    axs[0].legend()
    axs[1].legend()