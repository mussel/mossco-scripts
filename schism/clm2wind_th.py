import netCDF4
import numpy as np
import glob
import datetime
import sys
import netcdftime
import os
import pandas as pd

def convert_clm2wind_th(monthfile):

  ncin = netCDF4.Dataset(monthfile, "r")
  var = ncin.variables['time']

  utime = netcdftime.utime(var.units)
  time = utime.num2date(list(var[:]))

  btime = netcdftime.utime(f'seconds since {time[0]}')


  df = pd.DataFrame() #columns={'time','u','v'})

  df['time'] = btime.date2num(time)
  df['u'] = np.mean(ncin.variables["U_10M"][:,:,:], axis=(1,2,3))
  df['v'] = np.mean(ncin.variables["V_10M"][:,:,:], axis=(1,2,3))

  df.to_csv('wind.th', index=False, header=False, sep=' ', float_format='%.2f')
  #ncin.variables["V_10M"][:,:,:]


if __name__ == "__main__":

  #inFile = sys.argv[1]
  inFile = '/Users/Lemmen/devel/mossco/setups/helgoland/clm.kse.2002-2005.2d.helgoland.nc'

  clm_pattern = inFile

  for monthfile in glob.glob(clm_pattern):
      convert_clm2wind_th(monthfile)