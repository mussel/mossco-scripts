import netCDF4
import numpy as np
import glob
import datetime
import sys
import netcdftime
import os

def convert_clm2sfluxes(monthfile):

  ncin = netCDF4.Dataset(monthfile, "r")
  var = ncin.variables['time']

  utime = netcdftime.utime(var.units)
  time = utime.num2date(list(var[:]))

  yearmonth = np.unique([t.year * 100 + t.month for t in time])

  for i,ym in enumerate(yearmonth):
      basetime = datetime.datetime(ym//100, ym%100, 1, 0)
      convert_clm2sflux(ncin, basetime, i+1)

  ncin.close()

def convert_clm2sflux(ncin, basetime, i):

    sflux_name = f"sflux_air_1.{i:04d}.nc"
    if not os.path.exists(sflux_name):
        ncout = netCDF4.Dataset(sflux_name, "w")
        create_dimensions(ncin, ncout, basetime)
        create_air(ncin, ncout, basetime)
        ncout.close()

    sflux_name = f"sflux_prc_1.{i:04d}.nc"
    if not os.path.exists(sflux_name):
        ncout = netCDF4.Dataset(sflux_name, "w")
        create_dimensions(ncin, ncout, basetime)
        create_prc(ncin, ncout, basetime)
        ncout.close()

    sflux_name = f"sflux_rad_1.{i:04d}.nc"
    if not os.path.exists(sflux_name):
        ncout = netCDF4.Dataset(sflux_name, "w")
        create_dimensions(ncin, ncout, basetime)
        create_rad(ncin, ncout, basetime)

        calc_parameterized_swr(ncout)
        ncout.close()

def specific_humidity(temperature, dewpoint_temperature, pressure):
    # for temperature in Kelvin, pressure in Pa
    # temp_cel = temperature - 273.15 # temp_cel to calculate vapor_pressure
    dewpoint_celsius = dewpoint_temperature - 273.15
    gas_water = 461.51
    gas_dryair = 287.058

    # calculation: s=p(water)/p(wetair) --> p(wetair)=p(dryair)+p(water)
    # p(water) = e/Rw*T ; p(dryair) = e/Rd*T (T in K, e = water vapor pressure)
    # Source: https://de.wikipedia.org/wiki/Luftfeuchtigkeit#Spezifische_Luftfeuchtigkeit
    # Source: https://www.schweizer-fn.de/lueftung/feuchte/feuchte.php
    # results in kg water/kg air

    density_water = saturation_vapor_pressure(dewpoint_celsius) \
        /  (gas_water * temperature)
    density_dryair = (pressure - saturation_vapor_pressure(dewpoint_celsius)) \
        / (gas_dryair * temperature)
    density_wetair = density_dryair + density_water
    return  np.max(density_water / density_wetair, 1)

def saturation_vapor_pressure(temperature, formula='Huang'):
    """
    Calculate the saturation vapor pressure over ice and water according
    to the new formulation by Huang (2018, https://doi.org/10.1175/JAMC-D-17-0334.1)
    or the improved Magnus formula.

    Requires temperature in degree Celsius as input and returns saturation
    vapor pressure in Pa
    """

    magnus_wa, magnus_wb, magnus_wc = [17.625, 243.04, 610.94]
    magnus_ia, magnus_ib, magnus_ic = [22.587, 273.86, 611.21]

    huang_wa, huang_wb, huang_wc, huang_wd1, huang_wd2 = [
      34.494, 4924.99, 1.57, 237.1, 105.0]
    huang_ia, huang_ib, huang_ic, huang_id1, huang_id2 = [
      43.494, 6545.8, 2.0, 278.0, 868]

    if formula == 'Magnus':
        svp = np.where(temperature > 0.0,
            magnus_wc * np.exp((magnus_wa*temperature)/(magnus_wb+temperature)),
            magnus_ic * np.exp((magnus_ia*temperature)/(magnus_ib+temperature)),
            )
    else: # default formula is Huang 2018
        svp = np.where(temperature > 0.0,
          np.exp(huang_wa - huang_wb / (temperature + huang_wd1)) /
            (temperature + huang_wd2)**huang_wc,
          np.exp(huang_ia - huang_ib / (temperature + huang_id1)) /
            (temperature + huang_id2)**huang_ic)

    return svp

def test_saturation_vapor_pressure():
    """
    Unit test for calculation of saturation vapor pressure
    using Table 1 and Table 2 from Huang 2018
    """

    temperature = np.array(range(-100,130,20),dtype=float)
    temperature[-1] = 0.01
    temperature=np.sort(temperature)
    pressure_huang = [0.001405,0.05477, 1.0814,12.841, 103.23,611.29,
        611.689,2339.32,7384.93,19946.1, 47415.0, 101417]

    assert (pressure_huang - saturation_vapor_pressure(temperature)) / pressure_huang < 0.01, "Saturation vapor pressure calculation"

def create_dimensions(ncin, ncout, basetime, bounds=None):

    var = ncin.variables["time"][:]

    ncout.createDimension("lat", ncin.dimensions["rlat"].size)
    ncout.createDimension("lon", ncin.dimensions["rlon"].size)
    ncout.createDimension("time") # record dimension time

    var = ncout.createVariable("time", np.float64, ("time"))
    var.axis = "T"
    var.base_date = np.array([basetime.year, basetime.month, basetime.day, 0], dtype=np.int16)

    for varname in ["lat", "lon"]:
        var = ncout.createVariable(varname, 'f4', ("lat", "lon"), fill_value=-9999.0)
        [var.setncattr(att,ncin.variables[varname].getncattr(att)) for att in ncin.variables[varname].ncattrs()]

    if bounds == None:
        ncout.variables["lat"][:] = ncin.variables["lat"][:]
        ncout.variables["lon"][:] = ncin.variables["lon"][:]
    else:
        ncout.variables["lat"][:] = ncin.variables["lat"][:]
        ncout.variables["lon"][:] = ncin.variables["lon"][:]


    #start=(7.66012, 53.738956)
    #stop =(7.458982, 54.177829)
    return



def create_air(ncin, ncout, basetime):

    itime = subset_time(ncin, ncout, basetime)

    for varname in ["prmsl", "uwind", "vwind", "stmp", "spfh"]:
        var = ncout.createVariable(varname, 'f4', ("time","lat", "lon"), fill_value=-9999.0)

    [ncout.variables["stmp"].setncattr(att,ncin.variables["T_2M"].getncattr(att))
        for att in ncin.variables["T_2M"].ncattrs()]
    ncout.variables["stmp"][:] = ncin.variables["T_2M"][itime,:,:] - 273.15
    ncout.variables["stmp"].units = "degree_C"
    ncout.variables["stmp"].standard_name = "air_temperature_at_2m"
    ncout.variables["stmp"].code = "T_2M"

    #[ncout.variables["spfh"].setncattr(att,ncin.variables["var168"].getncattr(att))
    #    for att in ncin.variables["var168"].ncattrs()]
    #ncout.variables["spfh"][:] = specific_humidity(ncin.variables["var167"][itime,:,:],
    #    ncin.variables["var168"][itime,:,:], ncin.variables["var151"][itime,:,:])
    #ncout.variables["spfh"].units = "kg kg-1"
    #ncout.variables["spfh"].standard_name = "specific_humidity"
    #ncout.variables["spfh"].code = "f(167,168,151)"

    [ncout.variables["uwind"].setncattr(att,ncin.variables["U_10M"].getncattr(att))
        for att in ncin.variables["U_10M"].ncattrs()]
    ncout.variables["uwind"][:] = ncin.variables["U_10M"][itime,:,:]
    [ncout.variables["vwind"].setncattr(att,ncin.variables["V_10M"].getncattr(att))
        for att in ncin.variables["V_10M"].ncattrs()]
    ncout.variables["vwind"][:] = ncin.variables["V_10M"][itime,:,:]

    [ncout.variables["prmsl"].setncattr(att,ncin.variables["PMSL"].getncattr(att))
        for att in ncin.variables["PMSL"].ncattrs()]
    ncout.variables["prmsl"][:] = ncin.variables["PMSL"][itime,:,:]
    ncout.variables["prmsl"].standard_name = "surface_air_pressure"
    ncout.variables["prmsl"].units = "Pa"

def subset_time(ncin, ncout, basetime):

    utime = netcdftime.utime(ncin.variables['time'].units)
    btime = netcdftime.utime("days since " + str(basetime))

    time, uniq = np.unique(utime.num2date(list(ncin.variables['time'][:])), return_index=True)
    nexttime = datetime.datetime(basetime.year, basetime.month + 1, 1, 0) if basetime.month < 12 else datetime.datetime(basetime.year + 1, 1, 1, 0)
    itime = (time >= basetime) & (time < nexttime)
    ncout.variables["time"].units =  btime.unit_string
    ncout.variables["time"][:] = btime.date2num(time[itime])

    # Return itime from *entire* time index
    itime = uniq[itime]

    return itime

def create_rad(ncin, ncout, basetime):

    itime = subset_time(ncin, ncout, basetime)
    var = ncout.createVariable("dswrf", 'f4', ("time","lat", "lon"), fill_value=-9999.0)
    [var.setncattr(att,ncin.variables["ASWDIR_S"].getncattr(att))
        for att in ncin.variables["ASWDIR_S"].ncattrs()]
    var[:] = ncin.variables["ASWDIR_S"][itime,:,:]
    var.units = "W m-2"
    var.standard_name = "downwelling_shortwave_radiation"
    var.code = "ASWDIR_S"

    var = ncout.createVariable("dlwrf", 'f4', ("time","lat", "lon"), fill_value=-9999.0)
    [var.setncattr(att,ncin.variables["ATHB_S"].getncattr(att))
        for att in ncin.variables["ATHB_S"].ncattrs()]
    var[:] = ncin.variables["ATHB_S"][itime,:,:]
    var.units = "W m-2"
    var.standard_name = "downwelling_longwave_radiation"
    var.code = "ATHB_S"

def calc_parameterized_swr(ncout):

    from clc_shortwave_radiation import swr_clouded

    utime = netcdftime.utime(ncout.variables['time'].units)
    time = utime.num2date(list(ncout.variables['time'][:]))


    lon = np.mean(ncout.variables['lon'][:])
    lat = np.mean(ncout.variables['lat'][:])

    var = ncout.variables["dswrf"]
    swr = swr_clouded(lon*np.pi/180, lat*np.pi/180, time, cover=0.1)

    var[:] =np.tile(swr,(2,2,1)).swapaxes(2,0)

def create_prc(ncin, ncout, basetime):

    itime = subset_time(ncin, ncout, basetime)

    var = ncout.createVariable("prate", 'f4', ("time","lat", "lon"), fill_value=-9999.0)

    [var.setncattr(att,ncin.variables["TOT_PREC"].getncattr(att))
        for att in ncin.variables["TOT_PREC"].ncattrs()]
    var[:] = ncin.variables["TOT_PREC"][itime,:,:]
    var.units = "mm h-1"
    var.standard_name = "precipitation"
    var.code = "TOT_PREC"



if __name__ == "__main__":

  #inFile = sys.argv[1]
  inFile = '/Users/Lemmen/devel/mossco/setups/helgoland/clm.kse.2002-2005.2d.helgoland.nc'


  clm_pattern = inFile

  for monthfile in glob.glob(clm_pattern):
      convert_clm2sfluxes(monthfile)