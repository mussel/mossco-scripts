#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 16 19:46:04 2021

@author: Lemmen
"""

import pathlib
import sys
import schism
import pickle
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import cmocean

import netCDF4
import plot_schism_setup


vardict = {
 'albedo': {'cmap': cmocean.cm.gray, 'scale': 100.0},
 'include': {'cmap': cmocean.cm.gray_r, 'type':'int', },
 'diffmin':  {'cmap': cmocean.cm.matter},
 'TEM_nudge': {'cmap': cmocean.cm.gray_r, 'scale': 8640000.0,   'vlim': (0,100)},
 'watertype': {'cmap': cmocean.cm.phase,  'type': 'int', 'vlim': [1, 8]},
 'windrot_geo2proj': {'cmap': cmocean.cm.phase},
 'diffmax': {'cmap': cmocean.cm.tarn},
 'depths': {'cmap': cmocean.cm.deep},
 'depth': {'cmap': cmocean.cm.deep},
 'uvel': {'cmap': cmocean.cm.curl,'vlim': [-1, 1], 'units': 'm s-1'},
 'vvel': {'cmap': cmocean.cm.curl,'vlim': [-1, 1], 'units': 'm s-1'},
 'maecs_Chl': {'cmap': cmocean.cm.algae},
 'maecs_phyC': {'cmap': cmocean.cm.algae},
 'elev' : {'cmap': cmocean.cm.curl,'vlim': [-3, 3], 'units': 'm'},
 'diff_temp' : {'cmap': 'coolwarm','vlim': [-1, 1], 'units': 'K'},
 'diff_salt' : {'cmap': 'PiYG_r','vlim': [-.2, .2], 'units': 'PSU'}
}


def watertype_from_anomaly(setup, varid, nc):

    dims = varid.dimensions
    item = varid.name

    x = nc['SCHISM_hgrid_node_lon'][:] if 'SCHISM_hgrid_node_lon' in nc.variables else nc['SCHISM_hgrid_node_x'][:]
    y = nc['SCHISM_hgrid_node_lat'][:] if 'SCHISM_hgrid_node_lat' in nc.variables else nc['SCHISM_hgrid_node_y'][:]


    x = np.array(setup.lon) if hasattr(setup,'lon') else np.array(setup.x)
    y = np.array(setup.lat) if hasattr(setup,'lat') else np.array(setup.y)

    if not hasattr(setup, '.triangles'):
        setup.triangles=np.array(setup.nv) - 1
    #triangles = nc['SCHISM_hgrid_face_nodes'][:,:3]


    if not 'nSCHISM_hgrid_node' in dims: return

    time = [0]
    if 'time' in dims:
        time = nc.variables['time'][:] if 'time' in nc.variables else list(range(nc.dimensions['time'].size))

    cmap = vardict.get(item).get('cmap') if item in vardict and 'cmap' in vardict.get(item) else 'viridis'
    unit = vardict.get(item).get('units') if item in vardict and  'units' in vardict.get(item) else ''

    if len(dims) == 1:
        d = varid[:]
    elif len(dims) == 2 and dims[1] == 'nSCHISM_vgrid_layers':
        d = varid[:,-1] # surface layer
    elif dims[0] == 'time':
        if len(dims) == 2:
            d = varid[:,:]
        elif len(dims) == 3:
            d = varid[:,:,-1]

    (vmin, vmax) = vardict.get(item).get('vlim') if item in vardict and  'vlim' in vardict.get(item) else (np.min(d), np.max(d))

    for it, t in enumerate(time):

        if np.mod(it,6) > 0: continue

        if dims[0] == 'time':
            if len(dims) == 2:
                d = varid[it,:]
            elif len(dims) == 3:
                d = varid[it,:,-1]

        fig, ax = plt.subplots(1,1)

        plt.tripcolor(x, y, setup.triangles, d,
                  cmap=cmap, vmin=vmin, vmax=vmax)

        cb = plt.colorbar()
        cb.set_label(item + ' (' + unit + ')')
        plot_schism_setup.plot_inset_histogram(ax, d)

        #plt.savefig(f"schism_output_{item}.pdf")
        filename = f"schism_output_surface_{item}_{t}.png"
        print(f"Saving to {filename} ...")
        plt.savefig(filename, dpi=300)

        plt.close()

    watertype = (d*0.0 + 4).astype(int)
    watertype[d<-0.2]=7
    watertype[d<-0.4]=8
    setup.watertype=list(watertype)
    setup.dump_gr3_spat_var('/Users/Lemmen/temp/watertype.gr3',watertype,comment='gr3 by create-watertype_from_temp_anomaly.py')

def main():

    #output_file  = '/Users/Lemmen/setups/RUN01b/SCHISM/archive/schout_fabm0_maecs.nc'
    output_file = '/Users/Lemmen/temp/schout_diff.nc'

#    setup_dir = "/".join(pathlib.Path(output_file).parts[:-2])
    setup_dir = "/Users/Lemmen/setups/nsh/nsh-benjamin"
    #setup_dir = "/Users/Lemmen/setups/nsh/nsh-benjamin"
    #setup_dir = "/Users/Lemmen/devel/schism/schism_verification_tests/Test_CORIE"
    #setup_dir = "/Users/Lemmen/devel/schism/schism_verification_tests/Test_HeatConsv_TVD"

    if len(sys.argv)>1:
        setup_dir = sys.argv[1]

    setup_dir = pathlib.Path(setup_dir)
    hgrid_pickle = setup_dir / 'hgrid.pickle'
    hgrid_ll = setup_dir / 'hgrid.ll'
    hgrid_gr3 = setup_dir / 'hgrid.gr3'
    vgrid_in = setup_dir / 'vgrid.in'

    if hgrid_pickle.exists():
        with open(hgrid_pickle, 'rb') as f:
            print(f'Loading from {hgrid_pickle}')
            setup = pickle.load(f)

    ncid = netCDF4.Dataset(output_file, 'r')
    varid = ncid.variables.get('diff_temp')
    watertype_from_anomaly(setup, varid, ncid)

    return setup

if __name__ == "__main__":
    setup = main()