#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 16 19:46:04 2021

@author: Lemmen
"""

import pathlib
import sys
import schism
import pickle
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import cmocean

# !       Read in water type; the values for R, d_1, d_2 are given below
# !       solar flux= R*exp(z/d_1))+(1-R)*exp(z/d_2) (d_[1,2] are attentuation depths; smaller values for muddier water)
# !       1: 0.58 0.35 23 (Jerlov type I)
# !       2: 0.62 0.60 20 (Jerlov type IA)
# !       3: 0.67 1.00 17 (Jerlov type IB)
# !       4: 0.77 1.50 14 (Jerlov type II)
# !       5: 0.78 1.40 7.9 (Jerlov type III)
# !       6: 0.62 1.50 20 (Paulson and Simpson 1977; similar to type IA)
# !       7: 0.80 0.90 2.1 (Mike Z.'s choice for estuary)


vardict = {
 'albedo': {'cmap': cmocean.cm.gray, 'scale': 100.0,  'unit': '%'},
 'include': {'cmap': cmocean.cm.gray_r, 'type':'int', },
 'xlsc':  {'cmap': cmocean.cm.gray, 'unit': 'm'},
 'diffmin':  {'cmap': cmocean.cm.matter, 'unit': 'm2 s-1'},
 'drag':  {'cmap': cmocean.cm.matter, 'unit': ''}, # Cd drag coefficient
 'manning':  {'cmap': cmocean.cm.matter, 'unit': '1'}, # Manning's n
 'SAL_nudge': {'cmap': cmocean.cm.gray_r, 'scale': 8640000.0,  'unit': '% d-1', 'vlim': (0,100)},
 'TEM_nudge': {'cmap': cmocean.cm.gray_r, 'scale': 8640000.0,  'unit': '% d-1', 'vlim': (0,100)},
 'watertype': {'cmap': cmocean.cm.phase,  'type': 'int', 'vlim': [1, 7]},
 'windrot_geo2proj': {'cmap': cmocean.cm.phase},
 'diffmax': {'cmap': cmocean.cm.tarn, 'unit': 'm2 s-1'},
 'depths': {'cmap': cmocean.cm.deep, 'unit':'m'},
 'rough': {'cmap': cmocean.cm.deep, 'unit':'m'},
 'manning': {'cmap': cmocean.cm.deep, 'unit':'s m-(1/3)'},

}

def plot_inset_histogram(ax, data):

    hax=plt.axes([0.5,0.22,0.29,0.25])
    num, bins, patches = hax.hist(data, bins=50, log=False,
        facecolor='grey', edgecolor=None, alpha=0.6, density = 1, rwidth=1)

    stddev=np.std(data)
    mean=np.mean(data)
    percentiles=np.percentile(data,[5,95])


    plt.setp(hax.axes.yaxis,'ticks_position','right')
    plt.setp(hax.axes.yaxis,'label_position','right')

    plt.setp(hax,'yticklabels',[])
    plt.setp(hax,'yticks',[])
    hax.spines['top'].set_visible(False)
    hax.spines['right'].set_visible(False)
    hax.spines['left'].set_visible(False)
    hax.patch.set_visible(False)

    plt.setp(hax,'xticks',percentiles)
    xtl=hax.get_xticklabels()
    xtl[0]=u'%.1f'%(percentiles[0])
    xtl[1]=u'%.1f'%(percentiles[1])
    hax.set_xticklabels(xtl)

    for i,label in enumerate(hax.get_xticklabels()):
        label.set_fontsize(14)
        label.set_color('grey')

    bax=hax.twinx()
    pos1 = bax.get_position() # get the original position
    bax.set_position([pos1.x0, pos1.y0 - 0.05,  pos1.width, pos1.height]) # set a new position
    bp=bax.boxplot(data, vert=False, showfliers=False)
    bax.text(mean,0.75,u'%.1f\u00B1%.1f'%(mean,stddev),fontsize=12,ha='center')

    for key in bp.keys(): plt.setp(bp[key],'color','k')
    bax.axis('off')

    return


def plot_node_item(setup, item):

    fig, ax = plt.subplots(1,1)

    cmap = vardict.get(item).get('cmap') if item in vardict and 'cmap' in vardict.get(item) else 'viridis'
    data = getattr(setup, item)


    (vmin, vmax) = vardict.get(item).get('vlim') if item in vardict and  'vlim' in vardict.get(item) else (np.min(data), np.max(data))
    unit = vardict.get(item).get('unit') if item in vardict and  'unit' in vardict.get(item) else ''

    try:
      plt.tripcolor(setup.lon,setup.lat, setup.triangles, data,
                  cmap=cmap, vmin=vmin, vmax=vmax)
    except AttributeError:
      plt.tripcolor(setup.x,setup.y, setup.triangles, data,
                  cmap=cmap, vmin=vmin, vmax=vmax)

    cb = plt.colorbar()
    cb.set_label(item + ' (' + unit + ')')
    plot_inset_histogram(ax, data)

    plt.savefig("schism_setup_" + item + ".pdf")
    plt.savefig("schism_setup_" + item + ".png", dpi=300)


def plot_pet(setup, dir):

    import pathlib
    item = 'pet'

    pet_files=list(pathlib.Path(dir).glob('local_to_global*'))
    pet = np.array(setup.x)*0
    for filename in pet_files:
        i = int(filename.stem.split('_')[-1])
        with open(filename,'r') as f:
            content=f.readlines()
            nele=int(content[2])
            nnodes=int(content[3+nele])
            nodes = [int(content[j+4+nele].split()[-1]) - 1 for j in range(0,nnodes)]
            #print(i,nele, nnodes,max(nodes))
            pet[nodes] = i

    fig, ax = plt.subplots(1,1)

    cmap = vardict.get(item).get('cmap') if item in vardict and 'cmap' in vardict.get(item) else 'tab20c'
    data = pet

    (vmin, vmax) = vardict.get(item).get('vlim') if item in vardict and  'vlim' in vardict.get(item) else (np.min(data), np.max(data))
    unit = vardict.get(item).get('unit') if item in vardict and  'unit' in vardict.get(item) else ''

    try:
      plt.tripcolor(setup.lon,setup.lat, setup.triangles, pet,
                  cmap=cmap, vmin=vmin, vmax=vmax)
    except AttributeError:
      plt.tripcolor(setup.x,setup.y, setup.triangles, pet,
                  cmap=cmap, vmin=vmin, vmax=vmax)

    nodetext = [f'{i+1}/{pet[i]}' for i in range(0,setup.nnodes)]
    plt.text(setup.lon,setup.lat,nodetext)#,fontsize=1,ha='center')

    cb = plt.colorbar()
    cb.set_label(item + ' (' + unit + ')')
    plot_inset_histogram(ax, data)

    plt.savefig("schism_setup_" + item + ".pdf")
    plt.savefig("schism_setup_" + item + ".png", dpi=300)

def plot_mesh(setup):

    plt.figure()

    try:
      plt.triplot(setup.lon,setup.lat,setup.triangles, lw=0.01)
    except AttributeError:
      plt.triplot(setup.x,setup.y,setup.triangles, lw=0.01)
    plt.savefig("schism_setup_mesh.pdf")

def plot_domain(setup):

    plt.figure()
    setup.plot_domain_boundaries()
    plt.savefig("schism_setup_boundaries.pdf")

def plot_area(setup):

    setup.proj_area()


def plot_resolution(setup):

    plt.figure()
    plt.tripcolor(setup.lon,setup.lat, setup.triangles,list(setup.resolution_by_nodes.values()))
    plt.colorbar()
    plt.savefig("schism_setup_resolution.pdf")

def plot_cfl(setup, dt=100):

    plt.figure()
    plt.tripcolor(setup.lon,setup.lat, setup.triangles,list(setup.compute_cfl(dt=dt)))
    plt.colorbar()
    plt.savefig(f"schism_setup_cfl_at_{dt}.pdf")

def add_triangles(setup):

    if not hasattr(setup, '.triangles'):
        setup.triangles=np.array(setup.nv) - 1

    return setup


def read_gr3(setup, name):

    filename = pathlib.Path(str(setup.hgrid_file).replace(
        'hgrid.gr3', name + '.gr3'))

    try:
        df = pd.read_csv(filename, header=None, skiprows=2, #index_col=0,
                         nrows=setup.nnodes, delim_whitespace=True, usecols=[3])
        return df[3]

    except:
        return None


def add_gr3(setup, item):

    if not hasattr(setup, item):

        value = read_gr3(setup, item)
        if str(type(value)) == "<class 'NoneType'>":
           value = np.ones((setup.nnodes), dtype=np.float)

        value = value * (vardict.get(item).get('scale') if item in vardict and 'scale' in vardict.get(item) else 1)

        if np.unique(value).size == 1:
            print(f'Item {item} has unique value {np.unique(value)}')

        elif item in vardict and 'type' in vardict.get(item):
            setattr(setup, item, value.astype(vardict.get(item).get('type')))
            print(f'Setup added item {item} with type {type(getattr(setup,item))}')

        else:
            setattr(setup, item, value)
            print(f'Setup added item {item} with default type {type(getattr(setup,item))}')

    return setup

def main():
    setup_dir = '/Users/Lemmen/devel/mossco-scripts/schism/RUN_cosdatIII_2008_ecosmo'
    #setup_dir = "/Users/Lemmen/setups/nsh/nsh-benjamin"
    #setup_dir = "/Users/Lemmen/devel/schism/schism_verification_tests/Test_CORIE"
    #setup_dir = "/Users/Lemmen/devel/schism/schism_verification_tests/Test_HeatConsv_TVD"

    if len(sys.argv)>1:
        setup_dir = sys.argv[0]

    if pathlib.Path('hgrid.pickle').exists():
        with open('hgrid.pickle', 'rb') as f:
            setup = pickle.load(f)
    else:
        setup = schism.schism_setup(pathlib.Path(setup_dir) / 'hgrid.gr3',
            ll_file = pathlib.Path(setup_dir) / 'hgrid.ll',
            vgrid_file=pathlib.Path(setup_dir) / 'vgrid.in'
        )

    setup = add_triangles(setup)

    items = []

    for item in [p.stem for p in setup.hgrid_file.parent.glob("*.gr3") if not str(p.stem).startswith('hgrid')]:
        #print(f"Adding gr3 for {item}")
        setup = add_gr3(setup, item)
        if hasattr(setup, item):
            items.append(item)


    #for item in items if item.endswith('_nudge'):


    with open('hgrid.pickle','wb') as f:
        pickle.dump(setup,f)

    #[plot_node_item(setup, item) for item in items]

    #plot_mesh(setup)
    #plot_domain(setup)
    #plot_resolution(setup)
    #plot_cfl(setup, dt=100)
    #plot_node_item(setup, 'depths')
    plot_pet(setup, 'outputs')

    return setup

if __name__ == "__main__":
    setup = main()